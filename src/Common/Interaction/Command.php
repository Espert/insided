<?php

/*
 * This file is part of the insided/common package.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace Insided\Common\Interaction;

/**
 * Base interface for application Commands
 *
 * @author Fernando Coca <fcocajimeno@gmail.com>
 */
interface Command extends Message
{
    /**
     * @return string
     */
    public static function successMessage(): string;
}
