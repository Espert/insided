<?php

/*
 * This file is part of the insided/post bounded context.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace spec\Insided\Post\Application\Service;

use Doctrine\ORM\EntityManager;
use Insided\Common\Application\TransactionalApplicationService;
use Insided\Post\Application\Service\CreatePostService;
use Insided\Post\Domain\Model\Repository\PostWriterRepository;
use PhpSpec\ObjectBehavior;

/**
 * @author Fernando Coca <fernando@creamplan.com>
 */
class CreatePostServiceSpec extends ObjectBehavior
{
    function let(PostWriterRepository $postWriterRepository, EntityManager $manager)
    {
        $postWriterRepository->manager()->willReturn($manager);
        $this->beConstructedWith($postWriterRepository);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(CreatePostService::class);
    }

    function it_is_a_transactional_app_service()
    {
        $this->shouldBeAnInstanceOf(TransactionalApplicationService::class);
    }
}
