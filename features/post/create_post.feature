@post
@create_post
Feature: Create post
  In order to publish messages in a forum
  As an user
  I want to be able to create new posts

  @api
  Scenario: Creating a post
    Given I am creating a post with title "post title" and message "post message"
    When I create the post
    Then I should see message "The post was created successfully"
    And I should see a new post with title "post title" and message "post message"

  @api
  Scenario: Creating a post with empty title
    Given I am creating a post with title "''" and message "post message"
    When I create the post
    Then I should see error "A not empty title for post must be provided"

  @api
  Scenario: Creating a post with empty message
    Given I am creating a post with title "post title" and message "''"
    When I create the post
    Then I should see error "A not empty message for post must be provided"

  @api
  Scenario: Creating a post with a long title
    Given I am creating a post with title "Quiere la boca exhausta vid, kiwi, piña y fugaz jamón. Fabio me exige, sin tapujos, que añada cerveza" and message "post message"
    When I create the post
    Then I should see error "Post title must have 100 characters or less"