<?php

/*
 * This file is part of the insided/post bounded context.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace Insided\Post\Domain\Model;

use Insided\Common\Domain\Model\AggregateRoot;

/**
 * Aggregate Root: Post
 *
 * @author Fernando Coca <fcocajimeno@gmail.com>
 */
class Post extends AggregateRoot
{
    /**
     * @var \Insided\Post\Domain\Model\PostTitle
     */
    private $title;

    /**
     * @var \Insided\Post\Domain\Model\PostMessage
     */
    private $message;

    /**
     * @var \DateTime
     */
    private $creationDate;

    /**
     * @param \Insided\Post\Domain\Model\PostId $postId
     * @param \Insided\Post\Domain\Model\PostTitle $title
     * @param \Insided\Post\Domain\Model\PostMessage $message
     *
     * @return \Insided\Post\Domain\Model\Post
     */
    public static function create(
        PostId $postId,
        PostTitle $title,
        PostMessage $message
    ): Post
    {
        $post = new self();

        $post->setId($postId);
        $post
            ->setCreationDate(new \DateTime())
            ->setTitle($title)
            ->setMessage($message);

        return $post;
    }

    /**
     * @return \Insided\Post\Domain\Model\PostMessage
     */
    public function message(): PostMessage
    {
        return $this->message;
    }

    /**
     * @return \Insided\Post\Domain\Model\PostTitle
     */
    public function title(): PostTitle
    {
        return $this->title;
    }

    /**
     * @return \DateTime
     */
    public function creationDate(): \DateTime
    {
        return $this->creationDate;
    }

    /**
     * @param \DateTime $creationDate
     *
     * @return \Insided\Post\Domain\Model\Post
     */
    private function setCreationDate(\DateTime $creationDate): Post
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * @param \Insided\Post\Domain\Model\PostMessage $message
     *
     * @return \Insided\Post\Domain\Model\Post
     */
    private function setMessage(PostMessage $message): Post
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @param \Insided\Post\Domain\Model\PostTitle $title
     *
     * @return \Insided\Post\Domain\Model\Post
     */
    private function setTitle(PostTitle $title): Post
    {
        $this->title = $title;

        return $this;
    }
}
