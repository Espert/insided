<?php

/*
 * This file is part of the insided/post bounded context.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace Insided\Post\UserInterface\Action\Api;

use Insided\Common\Action\ApiAction;
use Insided\Post\Application\Interaction\Query\DetailPostQuery;
use Insided\Post\Application\Interaction\Transformer\CreatePostTransformer;
use Insided\Post\Application\Presentation\Normalizer\PostNormalizer;
use Insided\Post\Application\Presentation\Representation\PostRepresentation;
use Insided\Post\Application\Service\CreatePostService;
use Insided\Post\Application\Service\DetailPostService;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Api Action: Create Post
 *
 * @author Fernando Coca <fcocajimeno@gmail.com>
 */
class CreatePostApiAction extends ApiAction
{
    /**
     * @var \Insided\Post\Application\Service\DetailPostService
     */
    private $detailPostService;

    /**
     * @var \Insided\Post\Application\Service\CreatePostService
     */
    private $createPostService;

    /**
     * @param \Insided\Post\Application\Service\CreatePostService $createPostService
     * @param \Insided\Post\Application\Service\DetailPostService $detailPostService
     * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
     */
    public function __construct(
        CreatePostService $createPostService,
        DetailPostService $detailPostService,
        RequestStack $requestStack
    )
    {
        $this->createPostService = $createPostService;
        $this->detailPostService = $detailPostService;

        parent::__construct($requestStack);
    }

    /**
     * @throws \InvalidArgumentException When response could not be converted to JSON.
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function __invoke(): ResponseInterface
    {
        $request = $this->request();

        try {
            /** @var \Insided\Post\Application\Interaction\Command\CreatePostCommand $createPostCommand */
            $createPostCommand = CreatePostTransformer::transform($request);
            $this->createPostService->execute($createPostCommand);
        } catch (\Throwable $throwable) {
            return $this->jsonErrorResponse($throwable);
        }

        $postDetail = $this
            ->detailPostService
            ->execute(
                new DetailPostQuery($createPostCommand->postId())
            );

        return $this->jsonResponse([
            'posts' => [
                $this->normalizePosts($postDetail),
            ],
        ], $createPostCommand::successMessage());
    }

    /**
     * @param \Insided\Post\Application\Presentation\Representation\PostRepresentation $postRepresentation
     *
     * @return array
     */
    private function normalizePosts(PostRepresentation $postRepresentation): array
    {
        return (new PostNormalizer())->normalize(
            $postRepresentation
        );
    }
}
