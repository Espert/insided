<?php

/*
 * This file is part of the insided/post bounded context.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace Insided\Post\Domain\Model;

use Insided\Common\Domain\Model\ValueObject;
use Insided\Post\Domain\Model\Exception\PostMessageIsEmpty;

/**
 * Value Object: Post message
 *
 * @author Fernando Coca <fcocajimeno@gmail.com>
 */
class PostMessage implements ValueObject
{
    /**
     * @var string
     */
    protected $message;

    /**
     * @throws \Insided\Post\Domain\Model\Exception\PostMessageIsEmpty When message is an empty string.
     *
     * @param string $message
     */
    public function __construct(string $message)
    {
        $this->setMessage($message);
    }

    /**
     * @return string
     */
    public function message(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     *
     * @throws \Insided\Post\Domain\Model\Exception\PostMessageIsEmpty WHen message is an empty string.
     *
     * @return void
     */
    private function setMessage(string $message): void
    {
        if ($message === '') {
            throw new PostMessageIsEmpty();
        }

        $this->message = $message;
    }
}
