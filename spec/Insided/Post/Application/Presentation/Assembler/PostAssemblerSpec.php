<?php

/*
 * This file is part of the insided/post bounded context.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace spec\Insided\Post\Application\Presentation\Assembler;

use Insided\Common\Domain\Model\AggregateRoot;
use Insided\Common\Presentation\Assembler;
use Insided\Post\Application\Presentation\Assembler\PostAssembler;
use Insided\Post\Application\Presentation\Representation\PostRepresentation;
use Insided\Post\Domain\Model\Post;
use Insided\Post\Domain\Model\PostId;
use Insided\Post\Domain\Model\PostMessage;
use Insided\Post\Domain\Model\PostTitle;
use PhpSpec\ObjectBehavior;

/**
 * @author Fernando Coca <fernando@creamplan.com>
 */
class PostAssemblerSpec extends ObjectBehavior
{

    function it_is_initializable()
    {
        $this->shouldHaveType(PostAssembler::class);
    }

    function it_is_an_assembler()
    {
        $this->shouldBeAnInstanceOf(Assembler::class);
    }
    function it_should_assemble_only_posts(AggregateRoot $aggregateRoot)
    {
        $this->assemble($aggregateRoot)->shouldReturn(null);
    }

    function it_should_assemble_a_post()
    {
        $post = Post::create(
            new PostId('a5534de8-0f40-4cad-8912-38302cb5f08b'),
            new PostTitle('post title'),
            new PostMessage('post message')
        );

        $createdOn = $post->creationDate()->format('Y-m-d\TH:i:s');
        $assembled = $this->assemble($post);

        $assembled->shouldHaveType(PostRepresentation::class);
        $assembled->id()->shouldReturn('a5534de8-0f40-4cad-8912-38302cb5f08b');
        $assembled->title()->shouldReturn('post title');
        $assembled->message()->shouldReturn('post message');
        $assembled->createdOn()->shouldReturn($createdOn);
    }

}
