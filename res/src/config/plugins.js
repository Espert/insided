import VueMaterial from 'vue-material';
import VueRouter from 'vue-router';
import 'vue-material/dist/vue-material.css';
import VeeValidate from 'vee-validate';
import Vuex from 'vuex';

export default (Vue, ...params) => {
  params.filter(el => typeof el === 'object')
    .map(le => Vue.use(le));
  Vue.use(VueMaterial);
  Vue.use(Vuex);
  Vue.use(VueRouter);
  Vue.use(VeeValidate);
  Vue.material.registerTheme('default', {
    primary: 'pink',
    accent: 'pink',
    warn: 'red',
    background: 'white',
  });
};
