<?php

/*
 * This file is part of the insided/post bounded context.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace Insided\Post\Domain\Model;

use Insided\Common\Domain\Model\Identity;

/**
 * Domain Identity: Post
 *
 * @author Fernando Coca <fcocajimeno@gmail.com>
 */
class PostId extends Identity
{
}
