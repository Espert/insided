<?php

/*
 * This file is part of the insided/common package.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace Insided\Common\Testing\SymfonyDoctrine;

use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\ORM\Tools\SchemaTool;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Kernel;

class PHPUnitSymfonyDoctrineTestCase extends \PHPUnit\Framework\TestCase
{
    protected static $kernelBooted = false;
    protected static $schemaCreated = false;

    /**
     * @var Kernel
     */
    protected static $kernel;

    /**
     * @var ContainerInterface
     */
    protected static $container;

    /**
     * @var \Doctrine\Common\Persistence\ManagerRegistry
     */
    protected static $doctrine;

    /**
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    protected static $manager;

    public function setUpSchema()
    {
        if (null === self::$schemaCreated || self::$schemaCreated === false) {
            static::$doctrine = $this->createDoctrineRegistry();
            static::$manager = static::$doctrine->getManager($this->managerName());

            if ($metadata = $this->getMetadata()) {
                $schemaTool = new SchemaTool(static::$manager);
                $schemaTool->dropSchema($metadata);
                $schemaTool->createSchema($metadata);
            }
            self::$schemaCreated = true;
        } else {
            static::$doctrine->resetManager($this->managerName());
        }
    }

    public function purgeAll()
    {
        $managers =  static::$doctrine->getManagers();

        foreach ($managers as $manager) {
            $this->purge($manager);
        }
    }

    /**
     * @before
     */
    protected function setUpManager()
    {
        $this->setUpSchema();
        $this->purgeAll();
        $fixtures = $this->fixtures();
        $this->loadFixtureClasses($fixtures, static::$manager);
    }

    /**
     * Returns all fixture class names to load
     *
     * Override it to load desired fixtures
     *
     * @return array
     */
    protected function fixtures() : array
    {
        return [];
    }

    /**
     * Override it to work the desired entity manager
     *
     * @return string|null
     */
    protected function managerName()
    {
        return null;
    }

    /**
     * Returns all metadata by default.
     *
     * Override to only build selected metadata.
     * Return an empty array to prevent building the schema.
     *
     * @return array
     */
    protected function getMetadata()
    {
        return static::$manager->getMetadataFactory()->getAllMetadata();
    }

    /**
     * Returns all metadata by default.
     *
     * Override to only build selected metadata.
     * Return an empty array to prevent building the schema.
     *
     * @return array
     */
    protected static function metadata()
    {
        return static::$manager->getMetadataFactory()->getAllMetadata();
    }

    /**
     * Override to build doctrine registry yourself.
     *
     * By default a Symfony container is used to create it. It requires the SymfonyKernel trait.
     *
     * @throws \RuntimeException
     *
     * @return \Doctrine\Common\Persistence\ManagerRegistry
     */
    protected function createDoctrineRegistry()
    {
        if (isset(static::$container)) {
            return static::$container->get('doctrine');
        }

        throw new \RuntimeException(sprintf('Override %s to create a ManagerRegistry or use the SymfonyKernel trait.', __METHOD__));
    }

    /**
     * @afterClass
     */
    public static function shutDownDoctrine()
    {
    }

    /**
     * @beforeClass
     */
    public static function setUpSymfonyKernel()
    {
        if (false === static::$kernelBooted) {
            static::$kernel = static::createKernel();
            static::$kernel->boot();
            static::$container = static::$kernel->getContainer();
            static::$kernelBooted = true;
        }
    }

    /**
     * @param array $options
     *
     * @return \Insided\Kernel
     */
    protected static function createKernel(array $options = [])
    {
        $class = static::getKernelClass();
        $options = array_merge(static::getKernelOptions(), $options);
        return new $class(
            isset($options['environment']) ? $options['environment'] : 'test',
            isset($options['debug']) ? $options['debug'] : true
        );
    }

    /**
     * @return string
     */
    protected static function getKernelClass() : string
    {
        return \Insided\Kernel::class;
    }

    /**
     * @return array
     */
    protected static function getKernelOptions() : array
    {
        return ['environment' => 'test', 'debug' =>true];
    }

    protected function service(string $name)
    {
        return static::$container->get($name);
    }

    /**
     * Load a data fixture class
     *
     * @param string                               $className Class name of fixture
     * @param \Doctrine\ORM\EntityManagerInterface          $manager
     * @param \Doctrine\Common\DataFixtures\Loader $loader    Data fixtures loader

     */
    public function loadFixtureClass($className, $manager, $loader = null)
    {
        $notLoader = false;
        if (null === $loader) {
            $loader = $this->getLoader();
            $notLoader = true;
        }

        $fixture = new $className();
        if ($loader->hasFixture($fixture)) {
            unset($fixture);
            return;
        }

        $loader->addFixture(new $className);
        if ($fixture instanceof DependentFixtureInterface) {
            foreach ($fixture->getDependencies() as $dependency) {
                $this->loadFixtureClass($loader, $dependency);
            }
        }

        if ($notLoader) {
            $this->load($loader, $manager);
        }
    }
    /**
     * Load a data fixture class
     *
     * @param array                       $classNames Array of class names of fixtures
     * @param \Doctrine\ORM\EntityManagerInterface $manager
     */
    public function loadFixtureClasses(array $classNames, $manager)
    {
        $loader = $this->getLoader();

        foreach ($classNames as $className) {
            $this->loadFixtureClass($className, $manager, $loader);
        }

        $this->load($loader, $manager);
    }

    /**
     * @param \Doctrine\ORM\EntityManagerInterface $manager
     */
    public function purge($manager)
    {
        $purger = new ORMPurger();
        $purger->setPurgeMode(ORMPurger::PURGE_MODE_TRUNCATE);
        $executor = new ORMExecutor($manager, $purger);
        $executor->purge();
    }

    /**
     * @return Loader
     */
    private function getLoader()
    {
        return new Loader();
    }

    /**
     * @param $loader
     * @param $manager
     */
    private function load($loader, $manager)
    {
        $purger = new ORMPurger();
        $purger->setPurgeMode(ORMPurger::PURGE_MODE_TRUNCATE);
        $executor = new ORMExecutor($manager, $purger);
        $executor->purge();
        $executor->execute($loader->getFixtures(), true);
    }
}
