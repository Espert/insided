<?php

/*
 * This file is part of the insided/post bounded context.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace Insided\Post\Application\Interaction\Transformer;

use Insided\Common\Domain\IdentityGenerator;
use Insided\Common\Interaction\Message;
use Insided\Common\Interaction\Transformer;
use Insided\Post\Application\Interaction\Command\CreatePostCommand;
use Insided\Post\Domain\Model\PostId;
use Insided\Post\Domain\Model\PostMessage;
use Insided\Post\Domain\Model\PostTitle;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Transformer: Create Post Command
 *
 * @author Fernando Coca <fcocajimeno@gmail.com>
 */
class CreatePostTransformer implements Transformer
{
    /**
     * @param \Psr\Http\Message\ServerRequestInterface $request
     *
     * @throws \InvalidArgumentException If the generated UUID string is not a valid uuid.
     * @throws \Insided\Post\Domain\Model\Exception\PostMessageIsEmpty When post message is not provided or is empty.
     * @throws \Insided\Post\Domain\Model\Exception\PostTitleIsEmpty When post title is not provided or is empty.
     *
     * @return \Insided\Common\Interaction\Message
     */
    public static function transform(ServerRequestInterface $request): Message
    {
        $params = $request->getParsedBody();
        $postId = new PostId(IdentityGenerator::generate());
        $postTitle = \array_key_exists('title', $params)
            ? $params['title']
            : '';
        $postMessage = \array_key_exists('message', $params)
            ? $params['message']
            : '';

        return new CreatePostCommand(
            $postId,
            new PostTitle($postTitle),
            new PostMessage($postMessage)
        );
    }
}
