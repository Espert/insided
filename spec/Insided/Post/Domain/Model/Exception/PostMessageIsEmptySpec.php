<?php

/*
 * This file is part of the insided/post bounded context.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace spec\Insided\Post\Domain\Model\Exception;

use Insided\Post\Domain\Model\Exception\PostMessageIsEmpty;
use PhpSpec\ObjectBehavior;

/**
 * @author Fernando Coca <fernando@creamplan.com>
 */
class PostMessageIsEmptySpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(PostMessageIsEmpty::class);
    }

    function it_is_an_invalid_argument_exception()
    {
        $this->shouldBeAnInstanceOf(\InvalidArgumentException::class);
    }

    function it_should_have_a_message()
    {
        $this->getMessage()->shouldReturn('A not empty message for post must be provided');
    }
}
