<?php

/*
 * This file is part of the insided/post bounded context.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace Insided\Post\Infrastructure\Persistence\Doctrine\Fixture;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Insided\Post\Domain\Model\Post;
use Insided\Post\Domain\Model\PostId;
use Insided\Post\Domain\Model\PostMessage;
use Insided\Post\Domain\Model\PostTitle;

/**
 * Doctrine Fixture: Post Data
 *
 * @author Fernando Coca <fcocajimeno@gmail.com>
 */
class PostDataFixture extends AbstractFixture
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $post1 = Post::create(
            new PostId('a5284134-d1ab-4a99-90d5-c30f8b3fb023'),
            new PostTitle('Lorem ipsum dolor sit amet, consectetuer adipiscin'),
            new PostMessage('Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibu')
        );
        $reflection = new \ReflectionObject($post1);
        $property = $reflection->getProperty('creationDate');
        $property->setAccessible(true);
        $property->setValue($post1, new \DateTime());
        $property->setAccessible(false);
        $manager->persist($post1);

        $post2 = Post::create(
            new PostId('7b16cf4a-c37a-4325-ba69-c22435f1e926'),
            new PostTitle('Sed ut perspiciatis unde omnis iste natus error si'),
            new PostMessage('Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia vo')
        );
        $reflection = new \ReflectionObject($post2);
        $property = $reflection->getProperty('creationDate');
        $property->setAccessible(true);
        $property->setValue($post2, (new \DateTime())->add(new \DateInterval('P1D')));
        $property->setAccessible(false);
        $manager->persist($post2);

        $post3 = Post::create(
            new PostId('30f65bae-86de-4f30-8650-67660aa351d0'),
            new PostTitle('Li Europan lingues es membres del sam familie. Lor separat existentie es un myth'),
            new PostMessage('Li Europan lingues es membres del sam familie. Lor separat existentie es un myth. Por scientie, musica, sport etc, litot Europa usa li sam vocabular. Li lingues differe solmen in li grammatica, li pronunciation e li plu commun vocabules. Omnicos directe al desirabilite de un nov lingua franca: On refusa continuar payar custosi traductores. At solmen va esser necessi far uniform grammatica, pronunciation e plu sommun paroles. Ma quande lingues coalesce, li grammatica del resultant lingue es plu s')
        );
        $reflection = new \ReflectionObject($post3);
        $property = $reflection->getProperty('creationDate');
        $property->setAccessible(true);
        $property->setValue($post3, (new \DateTime())->add(new \DateInterval('PT2H')));
        $property->setAccessible(false);
        $manager->persist($post3);

        $post4 = Post::create(
            new PostId('91fc5497-9b03-4c82-87ac-48b6c22881c0'),
            new PostTitle('Ni siquiera los todopoderosos signos de puntuación dominan'),
            new PostMessage('Muy lejos, más allá de las montañas de palabras, alejados de los países de las vocales y las consonantes, viven los textos simulados. Viven aislados en casas de letras, en la costa de la semántica, un gran océano de lenguas. Un riachuelo llamado Pons fluye por su pueblo y los abastece con las normas necesarias. Hablamos de un país paraisomático en el que a uno le caen pedazos de frases asadas en la boca. Ni siquiera los todopoderosos signos de puntuación dominan a los textos simulados; una vida,')
        );
        $reflection = new \ReflectionObject($post4);
        $property = $reflection->getProperty('creationDate');
        $property->setAccessible(true);
        $property->setValue($post4, (new \DateTime())->add(new \DateInterval('P3D')));
        $property->setAccessible(false);
        $manager->persist($post4);

        $post5 = Post::create(
            new PostId('ffe60d4e-c053-401e-ac43-b25c3fe39d13'),
            new PostTitle('habiendo durado mucho el mal, el bien está ya cerca'),
            new PostMessage('Y, viéndole don Quijote de aquella manera, con muestras de tanta tristeza, le dijo: Sábete, Sancho, que no es un hombre más que otro si no hace más que otro. Todas estas borrascas que nos suceden son señales de que presto ha de serenar el tiempo y han de sucedernos bien las cosas; porque no es posible que el mal ni el bien sean durables, y de aquí se sigue que, habiendo durado mucho el mal, el bien está ya cerca. Así que, no debes congojarte por las desgracias que a mí me suceden, pues a ti no t')
        );

        $reflection = new \ReflectionObject($post5);
        $property = $reflection->getProperty('creationDate');
        $property->setAccessible(true);
        $property->setValue($post5, (new \DateTime())->sub(new \DateInterval('P1D')));
        $property->setAccessible(false);
        $manager->persist($post5);

        $post6 = Post::create(
            new PostId('855d9da1-f5bc-41f5-91c9-1b882aafcdfa'),
            new PostTitle('¿Qué me ha ocurrido? No estaba soñando'),
            new PostMessage('Una mañana, tras un sueño intranquilo, Gregorio Samsa se despertó convertido en un monstruoso insecto. Estaba echado de espaldas sobre un duro caparazón y, al alzar la cabeza, vio su vientre convexo y oscuro, surcado por curvadas callosidades, sobre el que casi no se aguantaba la colcha, que estaba a punto de escurrirse hasta el suelo. Numerosas patas, penosamente delgadas en comparación con el grosor normal de sus piernas, se agitaban sin concierto. - ¿Qué me ha ocurrido? No estaba soñando. Su.')
        );

        $reflection = new \ReflectionObject($post6);
        $property = $reflection->getProperty('creationDate');
        $property->setAccessible(true);
        $property->setValue($post6, (new \DateTime())->sub(new \DateInterval('PT1H')));
        $property->setAccessible(false);
        $manager->persist($post6);

        $manager->flush();
    }
}
