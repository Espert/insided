<?php

/*
 * This file is part of the insided/common package.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace Insided\Common\Application;

use Doctrine\ORM\EntityManager;
use Insided\Common\Interaction\Message;

/**
 * Base class for transactional application services
 *
 * @author Fernando Coca <fcocajimeno@gmail.com>
 */
abstract class TransactionalApplicationService
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    /**
     * @param \Doctrine\ORM\EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param \Insided\Common\Interaction\Message|null $message
     *
     * @return void
     */
    public function execute(?Message $message = null): void
    {
        $operation = function () use ($message) {
            $this->executeWith($message);
        };

        $this->entityManager->transactional($operation);
    }

    /**
     * @param \Insided\Common\Interaction\Message|null $message
     *
     * @return void
     */
    abstract protected function executeWith(?Message $message = null): void;
}
