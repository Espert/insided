<?php

/*
 * This file is part of the insided/post bounded context.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace spec\Insided\Post\Domain\Model;

use Insided\Common\Domain\Model\ValueObject;
use Insided\Post\Domain\Model\Exception\PostMessageIsEmpty;
use Insided\Post\Domain\Model\PostMessage;
use PhpSpec\ObjectBehavior;

/**
 * @author Fernando Coca <fernando@creamplan.com>
 */
class PostMessageSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith('post message');
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(PostMessage::class);
    }

    function it_is_a_value_object()
    {
        $this->shouldBeAnInstanceOf(ValueObject::class);
    }

    function it_should_have_a_message()
    {
        $this->message()->shouldReturn('post message');
    }

    function its_message_should_not_be_empty()
    {
        $this
            ->shouldThrow(new PostMessageIsEmpty())
            ->during(
                '__construct',
                [
                    '',
                ]
            );
    }
}
