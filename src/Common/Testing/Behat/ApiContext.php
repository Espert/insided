<?php

/*
 * This file is part of the insided/common package.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace Insided\Common\Testing\Behat;

use Assert\Assertion as Assert;
use Symfony\Component\Yaml\Yaml;

trait ApiContext
{
    protected $params;
    protected $request;
    protected $response;
    protected $url;
    protected $client;
    protected $files;
    protected $headers;
    public static $cliente;

    /**
     * @Then I should see error :arg1
     */
    public function iShouldSeeError($error)
    {
        $res = $this->parseJson($this->response->getBody());
        $code = $this->response->getStatusCode();

        Assert::same($res['status']['type'], 'error');
        Assert::same($res['status']['text'], $error);
        Assert::inArray($code, [400, 401, 403, 404]);
    }

    /**
     * @Then I should see error not found
     */
    public function iShouldSeeErrorNotFound()
    {
        $code = $this->response->getStatusCode();
        Assert::same($code, 404);
    }

    /**
     * Checks, that current page response status is equal to specified.
     *
     * @Then /^The status code should be (?P<code>\d+)$/
     */
    public function assertResponseStatus($code)
    {
        Assert::eq($this->response->getStatusCode(), $code);
    }

    /**
     * @Then I should see message :message
     */
    public function iShouldSeeMessage($message)
    {
        $res = $this->parseJson($this->response->getBody());
        $code = $this->response->getStatusCode();

        Assert::same($res['status']['type'], 'success');
        Assert::same($res['status']['text'], $message);
        Assert::same($res['status']['code'], 2000);
        Assert::inArray($code, [200, 201]);
    }

    protected function getUrl($params = array(), $url = '')
    {

        $url =  $this->generateUrl(
            $url !== '' ? $url : $this->url,
            array_merge(array(), $params),
            false
        );

        $url = str_replace("/app_test.php/" ,"", $url);

        return 'http://localhost/'.$url;
    }

    protected function addFormParams($array)
    {
        if ($this->params === null) {
            $this->params = [
                'form_params' => $array,
            ];
        } else {
            $this->params['form_params'] = $array;
        }
        $this->params['curl'] = [
            CURLOPT_TCP_NODELAY => 1,
        ];
    }

    protected function addXmlHeader()
    {
        if (! array_key_exists('headers', $this->params)) {
            $this->params['headers'] = [];
        }

        $this->params['headers']['accept'] = 'text/xml';
    }

    protected function addYmlHeader()
    {
        if (! array_key_exists('headers', $this->params)) {
            $this->params['headers'] = [];
        }

        $this->params['headers']['accept'] = 'text/x-yml';
    }

    protected function parseJson($json)
    {
        return \json_decode((string)$json, true);
    }

    protected function parseYml($yml)
    {
        return Yaml::parse($yml);
    }

    protected function parseXml($xml)
    {
        return simplexml_load_string($xml);
    }

    protected function send($url, $method = 'POST', array $params = null)
    {
        $client = $this->getHttpClient();

        if (null !== $params)
        {
            $auxParams = $this->params;
            $auxParams['form_params'] = array_key_exists('form_params', $this->params) && is_array($this->params['form_params'])
                ? array_merge($this->params['form_params'], $params)
                : $params;
            $params = $auxParams;
        }

        $params = null !== $params
            ? $params
            : $this->params;

        try {
            switch($method)
            {
                case 'POST' :
                    $this->response = $client->post($url, $params);
                    break;
                case 'GET' :
                    $params['query'] = null !== $params && array_key_exists('form_params', $params)
                        ? $params['form_params']
                        : [];
                    $this->response = $client->get($url, $params);
                    break;
                case 'PUT' :
                    $this->response = $client->put($url, $params);
                    break;
                case 'DELETE' :
                    $this->response = $client->delete($url, $params);
                    break;
            }

        } catch(\Exception $e){
            $this->response = $e->getResponse();
        }
    }

    /**
     * @BeforeSuite
     */
    public static function beforeSuite()
    {
        self::$cliente = new \GuzzleHttp\Client();
    }

    protected function getHttpClient()
    {
        return self::$cliente;
    }

    protected function generateUrl($route, array $parameters = array(), $absolute = false)
    {
        $route = str_replace('api/', 'app_test.php/api/', $this->getService('router')->generate($route, $parameters, $absolute));

        return $this->locatePath($route);
    }

    protected function transformsToMultipart(array $params = [])
    {
        $multipart = [];
        foreach ($params as $key => $value) {

            if (is_array($value)) {
                foreach ($value as $k => $v) {
                    $multipart[] = [
                        'name' => $key.'['.$k.']',
                        'contents' => $v,
                    ];
                }
            } else {
                $multipart[] = [
                    'name' => $key,
                    'contents' => $value,
                ];
            }
        }

        return $multipart;
    }
}