<?php

/*
 * This file is part of the insided/common package.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace spec\Insided\Common\Domain\Model;

use Insided\Common\Domain\Model\Identity;
use PhpSpec\ObjectBehavior;

/**
 * @author Fernando Coca <fernando@creamplan.com>
 */
class IdentitySpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith('8fb5a564-eb56-481f-a126-c7cc11c93c00');
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(Identity::class);
    }

    function it_should_have_an_id()
    {
        $this->id()->shouldReturn('8fb5a564-eb56-481f-a126-c7cc11c93c00');
    }

    function it_should_compare_two_identities()
    {
        $this
            ->equals(new Identity('8fb5a564-eb56-481f-a126-c7cc11c93c00'))
            ->shouldReturn(true);
    }

    function it_should_not_be_created_with_an_invalid_uuid()
    {
        $this
            ->shouldThrow(
                new \InvalidArgumentException(
                    'The identity "99aff663-bd24-4" for class "Insided\Common\Domain\Model\Identity" is not a valid UUID'
                )
            )
            ->during(
                '__construct',
                [
                    '99aff663-bd24-4',
                ]
            );
    }
}
