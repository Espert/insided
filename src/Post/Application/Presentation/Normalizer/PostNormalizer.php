<?php

/*
 * This file is part of the insided/post bounded context.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace Insided\Post\Application\Presentation\Normalizer;

use Insided\Common\Presentation\Normalizer;
use Insided\Common\Presentation\Representation;
use Insided\Post\Application\Presentation\Representation\PostRepresentation;

/**
 * Normalizer: Post
 *
 * @author Fernando Coca <fcocajimeno@gmail.com>
 */
class PostNormalizer implements Normalizer
{
    /**
     * @param \Insided\Common\Presentation\Representation $representation
     *
     * @return array|null
     */
    public function normalize(Representation $representation): ?array
    {
        if (! $representation instanceof PostRepresentation) {
            return null;
        }

        return [
            'id' => $representation->id(),
            'title' => $representation->title(),
            'message' => $representation->message(),
            'createdOn' => $representation->createdOn(),
        ];
    }
}
