<?php

/*
 * This file is part of the insided/post bounded context.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace spec\Insided\Post\Domain\Model\Exception;

use Insided\Post\Domain\Model\Exception\PostTitleIsEmpty;
use Insided\Post\Domain\Model\Exception\PostTitleIsTooLong;
use PhpSpec\ObjectBehavior;

/**
 * @author Fernando Coca <fernando@creamplan.com>
 */
class PostTitleIsTooLongSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(PostTitleIsTooLong::class);
    }

    function it_is_an_invalid_argument_exception()
    {
        $this->shouldBeAnInstanceOf(\InvalidArgumentException::class);
    }

    function it_should_have_a_message()
    {
        $this->getMessage()->shouldReturn('Post title must have 100 characters or less');
    }
}
