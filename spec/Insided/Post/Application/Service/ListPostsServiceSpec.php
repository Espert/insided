<?php

/*
 * This file is part of the insided/post bounded context.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace spec\Insided\Post\Application\Service;

use Insided\Common\Application\ApplicationService;
use Insided\Common\Interaction\Message;
use Insided\Post\Application\Service\ListPostsService;
use Insided\Post\Domain\Model\Repository\PostReaderRepository;
use PhpSpec\ObjectBehavior;

/**
 * @author Fernando Coca <fcocajimeno@gmail.com>
 */
class ListPostsServiceSpec extends ObjectBehavior
{
    function let(PostReaderRepository $postReaderRepository)
    {
        $this->beConstructedWith($postReaderRepository);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(ListPostsService::class);
    }

    function it_is_an_app_service()
    {
        $this->shouldBeAnInstanceOf(ApplicationService::class);
    }

    function it_should_execute_only_list_posts_queries(Message $message)
    {
        $this
            ->shouldThrow(new \RuntimeException('A "ListPostsQuery" must be provided to execute "ListPostsService"'))
            ->during('execute', [$message]);
    }
}
