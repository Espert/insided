<?php

namespace Post;

use Insided\Common\Testing\Behat\ApiContext;
use FeatureContext;
use Assert\Assertion as Assert;
use Insided\Common\Testing\Behat\DoctrineFixturesContext;
use Insided\Post\Infrastructure\Persistence\Doctrine\Fixture\PostDataFixture;

/**
 * This context class contains the definitions of the steps used by the demo
 * feature file. Learn how to get started with Behat and BDD on Behat's website.
 *
 * @see http://behat.org/en/latest/quick_start.html
 */
class ListPostsApiContext extends FeatureContext
{
    use ApiContext;
    use DoctrineFixturesContext;

    private static $init = false;
    private $listPostsUrl = 'insided_post_action_api_list_posts';

    /**
     * @BeforeScenario
     */
    public function prepare()
    {
        if (!self::$init) {
            $this->buildSchema();
            self::$init = true;
        }

        $this->loadFixtureClasses([
           PostDataFixture::class,
        ]);
    }

    /**
     * @Given I am listing posts with per page :arg1 and page :arg2
     */
    public function iAmListingPostsWithPerPageAndPage($arg1, $arg2)
    {
        $this->addFormParams([
            'per_page' => (int) $arg1,
            'page' => (int) $arg2,
        ]);
    }

    /**
     * @When I list posts
     */
    public function iListPosts()
    {
        $this->send($this->getUrl([], $this->listPostsUrl), 'GET');
    }

    /**
     * @Then I should see a post with title :arg1 in position :arg2
     */
    public function iShouldSeeAPostWithTitleAndMessageInPosition($arg1, $arg2)
    {
        $posts = $this->parseJson($this->response->getBody());
        Assert::same($posts['data']['posts'][(int) $arg2]['title'], $arg1);
    }
}
