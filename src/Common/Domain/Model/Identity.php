<?php

/*
 * This file is part of the insided/common package.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace Insided\Common\Domain\Model;

use Assert\Assertion as Assert;

/**
 * Base class for domain object identities
 *
 * @author Fernando Coca <fcocajimeno@gmail.com>
 */
class Identity
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @param string $stringId
     *
     * @throws \InvalidArgumentException If the given string is not a valid uuid.
     */
    public function __construct(string $stringId)
    {
        $this->setId($stringId);
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }

    /**
     * Check if the given object is equals to the actual object
     *
     * @param \Insided\Common\Domain\Model\Identity $anIdentity
     *
     * @return bool
     */
    public function equals(Identity $anIdentity): bool
    {
        if ($anIdentity !== null && \get_class($this) === \get_class($anIdentity)) {
            return $this->id() === $anIdentity->id();
        }

        return false;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string) $this->id;
    }

    /**
     * Delegates the validation to the specialized child classes
     *
     * @param string $stringId
     *
     * @return void
     */
    public function validateId(string $stringId): void
    {
    }

    /**
     * @param string $stringId
     *
     * @throws \InvalidArgumentException If the given string is not a valid uuid.
     *
     * @return void
     */
    protected function setId(string $stringId): void
    {
        try {
            Assert::uuid($stringId);
        } catch (\Assert\AssertionFailedException $e) {
            $class = \get_called_class();
            $message = "The identity \"${stringId}\" for class \"${class}\" is not a valid UUID";

            throw new \InvalidArgumentException($message);
        }

        $this->validateId($stringId);
        $this->id = $stringId;
    }
}
