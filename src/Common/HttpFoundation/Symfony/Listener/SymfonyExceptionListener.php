<?php

/*
 * This file is part of the insided/common package.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace Insided\Common\HttpFoundation\Symfony\Listener;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

/**
 * @author Fernando Coca <fcocajimeno@gmail.com>
 */
class SymfonyExceptionListener
{
    /**
     * @param \Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent $event
     *
     * @return void
     */
    public function onKernelException(GetResponseForExceptionEvent $event): void
    {
        $exception = $event->getException();

        $code = $exception instanceof HttpExceptionInterface
            ? $exception->getStatusCode()
            : 0;

        if ($code === 404) {
            $response = new RedirectResponse('/posts');
            $event->setResponse($response);
        }
    }
}
