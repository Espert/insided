import * as types from '../mutation-types';

// initial state
/* eslint no-shadow: 0, no-param-reassign: 0*/
const state = {
  message: '',
  errorMessage: '',
};

// getters
const getters = {};

// actions
const actions = {
  showMessage({ commit }, message) {
    commit(types.SHOW_MESSAGE, { message });
  },
  showErrorMessage({ commit }, message) {
    commit(types.SHOW_ERROR_MESSAGE, { message });
  },
};

// mutations
const mutations = {
  [types.SHOW_MESSAGE](state, { message }) {
    state.message = message;
  },
  [types.SHOW_ERROR_MESSAGE](state, { message }) {
    state.errorMessage = message;
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
