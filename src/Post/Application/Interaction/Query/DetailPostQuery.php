<?php

/*
 * This file is part of the insided/post bounded context.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace Insided\Post\Application\Interaction\Query;

use Insided\Common\Interaction\Query;
use Insided\Post\Domain\Model\PostId;
use Insided\Post\Domain\Model\PostMessage;
use Insided\Post\Domain\Model\PostTitle;

/**
 * Query: Detail Post
 *
 * @author Fernando Coca <fcocajimeno@gmail.com>
 */
class DetailPostQuery implements Query
{
    /**
     * @var \Insided\Post\Domain\Model\PostId
     */
    protected $postId;

    /**
     * @param \Insided\Post\Domain\Model\PostId $postId
     */
    public function __construct(PostId $postId)
    {
        $this->postId = $postId;
    }

    /**
     * @return string
     */
    public static function errorMessage(): string
    {
        return 'It was not possible to retrieve post detail';
    }

    /**
     * @return \Insided\Post\Domain\Model\PostId
     */
    public function postId(): PostId
    {
        return $this->postId;
    }
}
