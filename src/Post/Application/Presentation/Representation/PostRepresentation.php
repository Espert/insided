<?php

/*
 * This file is part of the insided/post bounded context.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace Insided\Post\Application\Presentation\Representation;

use Insided\Common\Presentation\Representation;

/**
 * Representation: Post
 *
 * @author Fernando Coca <fcocajimeno@gmail.com>
 */
class PostRepresentation implements Representation
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $message;

    /**
     * @var string
     */
    private $createdOn;

    /**
     * @param string $postId
     * @param string $title
     * @param string $message
     * @param string $createdOn
     */
    public function __construct(
        string $postId,
        string $title,
        string $message,
        string $createdOn
    )
    {
        $this->id = $postId;
        $this->title = $title;
        $this->message = $message;
        $this->createdOn = $createdOn;
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function message(): string
    {
        return $this->message;
    }

    /**
     * @return string
     */
    public function createdOn(): string
    {
        return $this->createdOn;
    }
}
