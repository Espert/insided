<?php

/*
 * This file is part of the insided/common package.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace spec\Insided\Common\Interaction;

use Insided\Common\Interaction\Message;
use Insided\Common\Interaction\Response;
use PhpSpec\ObjectBehavior;

/**
 * @author Fernando Coca <fcocajimeno@gmail.com>
 */
class ResponseSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(Response::class);
    }

    function it_is_a_message()
    {
        $this->shouldBeAnInstanceOf(Message::class);
    }

    function it_should_have_data()
    {
        $this->data()->shouldReturn([]);
    }

    function it_should_have_metadata()
    {
        $this->metadata()->shouldReturn([]);
    }
}
