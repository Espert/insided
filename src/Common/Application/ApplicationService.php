<?php

/*
 * This file is part of the insided/common package.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace Insided\Common\Application;

use Insided\Common\Interaction\Message;

/**
 * Base class for application services
 *
 * @author Fernando Coca <fcocajimeno@gmail.com>
 */
abstract class ApplicationService
{
    /**
     * @param \Insided\Common\Interaction\Message|null $message
     *
     * @return mixed
     */
    public function execute(?Message $message = null)
    {
        return $this->executeWith($message);
    }

    /**
     * @param \Insided\Common\Interaction\Message|null $message
     *
     * @return mixed
     */
    abstract protected function executeWith(?Message $message = null);
}
