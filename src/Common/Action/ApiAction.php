<?php

/*
 * This file is part of the insided/common package.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace Insided\Common\Action;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Symfony\Bridge\PsrHttpMessage\Factory\DiactorosFactory;
use Symfony\Component\HttpFoundation\RequestStack;
use Zend\Diactoros\Response\JsonResponse;

/**
 * Base class for API user interface actions
 *
 * @author Fernando Coca <fcocajimeno@gmail.com>
 */
abstract class ApiAction
{
    /**
     * @var \Symfony\Component\HttpFoundation\RequestStack
     */
    private $requestStack;

    /**
     * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    /**
     * @return \Psr\Http\Message\ResponseInterface
     */
    abstract public function __invoke(): ResponseInterface;

    /**
     * @param array $data
     * @param string $statusText
     * @param array $metadata
     * @param int $statusCode
     * @param string[] $headers
     *
     * @throws \InvalidArgumentException If unable to encode the $data to JSON.
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    protected function jsonResponse(
        array $data = [],
        string $statusText = 'ok',
        array $metadata = [],
        int $statusCode = 200,
        array $headers = []
    ): ResponseInterface
    {
        return new JsonResponse([
            'status' => [
                'type' => 'success',
                'code' => 2000,
                'text' => $statusText,
            ],
            'meta' => $metadata,
            'data' => $data,
        ], $statusCode, $headers);
    }

    /**
     * @param \Throwable $throwable
     * @param int $statusCode
     * @param string[] $headers
     *
     * @throws \InvalidArgumentException If unable to encode the $data to JSON.
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    protected function jsonErrorResponse(
        \Throwable $throwable,
        int $statusCode = 400,
        array $headers = []
    ): ResponseInterface
    {
        return new JsonResponse([
            'status' => [
                'type' => 'error',
                'code' => 4000,
                'text' => $throwable->getMessage(),
            ],
            'meta' => [],
            'data' => [],
        ], $statusCode, $headers);
    }

    /**
     * @return \Psr\Http\Message\ServerRequestInterface
     */
    protected function request(): ServerRequestInterface
    {
        $psr7Factory = new DiactorosFactory();
        $symfonyRequest = $this->requestStack->getMasterRequest();

        return $psr7Factory->createRequest($symfonyRequest);
    }
}
