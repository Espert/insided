<?php

/*
 * This file is part of the insided/post bounded context.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace Insided\Post\Infrastructure\Persistence\Doctrine\Repository;

use Insided\Common\Domain\IdentityGenerator;
use Insided\Common\Testing\SymfonyDoctrine\PHPUnitSymfonyDoctrineTestCase;
use Insided\Post\Domain\Model\Post;
use Insided\Post\Domain\Model\PostId;
use Insided\Post\Domain\Model\PostMessage;
use Insided\Post\Domain\Model\PostTitle;
use Insided\Post\Domain\Model\Repository\PostReaderRepository;
use Insided\Post\Domain\Model\Repository\PostWriterRepository;
use Insided\Post\Infrastructure\Persistence\Doctrine\Fixture\PostDataFixture;

/**
 * @author Fernando Coca <fcocajimeno@gmail.com>
 */
class DoctrinePostReaderRepositoryTest extends PHPUnitSymfonyDoctrineTestCase
{
    /**
     * @return string
     */
    protected function managerName(): string
    {
        return 'insided';
    }

    /**
     * @return array
     */
    protected function fixtures(): array
    {
        return [
            PostDataFixture::class,
        ];
    }

    public function test_it_should_read_all_posts_ordered_by_creation_date()
    {
        $posts = $this->postReaderRepository()->allPosts(5);

        self::assertNotNull($posts);
        self::assertContainsOnlyInstancesOf(Post::class, $posts->data());
        self::assertCount(5, $posts->data());
        self::assertSame(6, $posts->metadata()['total']);
        self::assertSame(1, $posts->metadata()['page']);
        self::assertSame(2, $posts->metadata()['pages']);
        self::assertSame(5, $posts->metadata()['showing']);

        $actualDate = null;

        foreach ($posts->data() as $post) {
            if ($actualDate === null) {
                $actualDate = $post->creationDate();
            } else {
                self::assertTrue($actualDate > $post->creationDate());
            }
        }
    }

    public function test_it_should_read_a_post()
    {
        $postId = new PostId(IdentityGenerator::generate());
        $post = Post::create(
            $postId,
            new PostTitle('post title'),
            new PostMessage('post message')
        );

        $this->postWriterRepository()->add($post);
        $this->postWriterRepository()->manager()->flush();

        $postAdded = $this->postReaderRepository()->postOfId($postId);

        self::assertNotNull($postAdded);
        self::assertSame($postAdded->id()->id(), $postId->id());
    }

    private function postWriterRepository(): PostWriterRepository
    {
        return static::service('insided.post.repository.writer.post');
    }

    private function postReaderRepository(): PostReaderRepository
    {
        return static::service('insided.post.repository.reader.post');
    }
}
