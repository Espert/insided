<?php

/*
 * This file is part of the insided/post bounded context.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace spec\Insided\Post\Domain\Model;

use Insided\Common\Domain\Model\Identity;
use Insided\Post\Domain\Model\PostId;
use PhpSpec\ObjectBehavior;

/**
 * @author Fernando Coca <fernando@creamplan.com>
 */
class PostIdSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith('066a96e2-b1ae-462f-8917-6329940ba070');
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(PostId::class);
    }

    function it_is_an_identity()
    {
        $this->shouldBeAnInstanceOf(Identity::class);
    }

    function it_should_have_an_uuid()
    {
        $this->id()->shouldReturn('066a96e2-b1ae-462f-8917-6329940ba070');
    }
}
