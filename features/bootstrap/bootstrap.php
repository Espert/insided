<?php

use Symfony\Component\Dotenv\Dotenv;

// The check is to ensure we don't use .env in production
if (!getenv('APP_ENV')) {
    putenv("DATABASE_HOST=172.19.0.2");
    (new Dotenv())->load(__DIR__.'/../../.env');
}
