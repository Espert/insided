import titleMaxLength from '../common/validations/forum/post/title_max_length';

const messages = {
  required: () => 'The field is required',
};

const dictionary = {
  en: {
    messages,
  },
};

export default (Validator) => {
  Validator.extend('postTitleMaxLength', titleMaxLength);
  Validator.updateDictionary(dictionary);
};
