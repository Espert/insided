<?php

/*
 * This file is part of the insided/post bounded context.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace Insided\Post\Application\Service;

use Insided\Common\Application\ApplicationService;
use Insided\Common\Interaction\Message;
use Insided\Post\Application\Interaction\Query\DetailPostQuery;
use Insided\Post\Application\Presentation\Assembler\PostAssembler;
use Insided\Post\Application\Presentation\Representation\PostRepresentation;
use Insided\Post\Domain\Model\Repository\PostReaderRepository;

/**
 * Application Service: Detail Post
 *
 * @author Fernando Coca <fcocajimeno@gmail.com>
 */
class DetailPostService extends ApplicationService
{
    /**
     * @var \Insided\Post\Domain\Model\Repository\PostReaderRepository
     */
    private $postReaderRepository;

    /**
     * @var \Insided\Post\Application\Presentation\Assembler\PostAssembler
     */
    private $postAssembler;

    /**
     * @param \Insided\Post\Domain\Model\Repository\PostReaderRepository $postReaderRepository
     */
    public function __construct(PostReaderRepository $postReaderRepository)
    {
        $this->postReaderRepository = $postReaderRepository;
        $this->postAssembler = new PostAssembler();
    }

    /**
     * @param \Insided\Common\Interaction\Message|null $detailPostQuery
     *
     * @throws \RuntimeException When the message provided is not a DetailPostQuery.
     *
     * @return \Insided\Post\Application\Presentation\Representation\PostRepresentation|null
     */
    protected function executeWith(?Message $detailPostQuery = null): ?PostRepresentation
    {
        if (! $detailPostQuery instanceof DetailPostQuery) {
            throw new \RuntimeException(
                'A "DetailPostQuery" must be provided to execute "DetailPostService"'
            );
        }

        $post = $this->postReaderRepository->postOfId($detailPostQuery->postId());

        return $post !== null
            ? $this->postAssembler->assemble($post)
            : null;
    }
}
