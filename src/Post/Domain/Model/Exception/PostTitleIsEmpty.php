<?php

/*
 * This file is part of the insided/post bounded context.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace Insided\Post\Domain\Model\Exception;

/**
 * Domain Exception: Post title is empty
 *
 * @author Fernando Coca <fcocajimeno@gmail.com>
 */
class PostTitleIsEmpty extends \InvalidArgumentException
{
    public const MESSAGE = 'A not empty title for post must be provided';

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct(self::MESSAGE);
    }
}
