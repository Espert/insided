/* eslint-disable */
const PostCreation = (r) => require.ensure([], () => r(require('../spa/forum/post_creation/PostCreation')), 'base');
const PostList = (r) => require.ensure([], () => r(require('../spa/forum/post_list/PostList')), 'base');

export const routes = [
  {
    path: '/posts/creation',
    component: PostCreation,
  },  {
    path: '/posts',
    component: PostList,
  },
];

export const VueRouterObject = {
  routes,
  mode: 'history',
};

export default VueRouter => new VueRouter(VueRouterObject);
