<?php

/*
 * This file is part of the insided/post bounded context.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace Insided\Post\Infrastructure\Persistence\Doctrine\Repository;

use Doctrine\ORM\EntityManager;
use Insided\Post\Domain\Model\Post;
use Insided\Post\Domain\Model\Repository\PostWriterRepository;

/**
 * Domain Writer Repository: Post
 *
 * @author Fernando Coca <fcocajimeno@gmail.com>
 */
class DoctrinePostWriterRepository implements PostWriterRepository
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @param \Doctrine\ORM\EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param \Insided\Post\Domain\Model\Post $post
     *
     * @throws \RuntimeException When Post could not be added to repository.
     *
     * @return void
     */
    public function add(Post $post): void
    {
        try {
            $this->entityManager->persist($post);
        } catch (\Doctrine\ORM\ORMInvalidArgumentException $e) {
            throw new \RuntimeException('The Post could not be created');
        }
    }

    /**
     * @param \Insided\Post\Domain\Model\Post $post
     *
     * @throws \RuntimeException When Post could not be removed from repository.
     *
     * @return void
     */
    public function remove(Post $post): void
    {
        try {
            $this->entityManager->remove($post);
        } catch (\Doctrine\ORM\ORMInvalidArgumentException $e) {
            throw new \RuntimeException('The Post could not be removed');
        }
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    public function manager(): EntityManager
    {
        return $this->entityManager;
    }
}
