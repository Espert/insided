<?php

/*
 * This file is part of the insided/post bounded context.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace spec\Insided\Post\Domain\Model;

use Insided\Common\Domain\Model\ValueObject;
use Insided\Post\Domain\Model\Exception\PostTitleIsEmpty;
use Insided\Post\Domain\Model\Exception\PostTitleIsTooLong;
use Insided\Post\Domain\Model\PostTitle;
use PhpSpec\ObjectBehavior;

/**
 * @author Fernando Coca <fernando@creamplan.com>
 */
class PostTitleSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith('post title');
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(PostTitle::class);
    }

    function it_is_a_value_object()
    {
        $this->shouldBeAnInstanceOf(ValueObject::class);
    }

    function it_should_have_a_title()
    {
        $this->title()->shouldReturn('post title');
    }

    function its_title_should_not_be_empty()
    {
        $this
            ->shouldThrow(new PostTitleIsEmpty())
            ->during(
                '__construct',
                [
                    '',
                ]
            );
    }

    function its_title_should_not_be_longer_than_100_characters()
    {
        $this
            ->shouldThrow(new PostTitleIsTooLong())
            ->during(
                '__construct',
                [
                    'Quiere la boca exhausta vid, kiwi, piña y fugaz jamón. Fabio me exige, sin tapujos, que añada cerveza',
                ]
            );
    }
}
