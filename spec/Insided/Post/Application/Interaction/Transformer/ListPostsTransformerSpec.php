<?php

/*
 * This file is part of the insided/post bounded context.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace spec\Insided\Post\Application\Interaction\Transformer;

use Insided\Common\Interaction\Transformer;
use Insided\Post\Application\Interaction\Query\ListPostsQuery;
use Insided\Post\Application\Interaction\Transformer\ListPostsTransformer;
use PhpSpec\ObjectBehavior;
use Symfony\Bridge\PsrHttpMessage\Factory\DiactorosFactory;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Fernando Coca <fernando@creamplan.com>
 */
class ListPostsTransformerSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(ListPostsTransformer::class);
    }

    function it_is_a_message_transformer()
    {
        $this->shouldBeAnInstanceOf(Transformer::class);
    }

    function it_should_transform_a_request_to_list_posts_query()
    {
        $psr7Factory = new DiactorosFactory();
        $symfonyRequest = Request::create(
            'https://insided.com?per_page=15&page=2',
            'GET'
        );
        $psrRequest = $psr7Factory->createRequest($symfonyRequest);

        $query = static::transform($psrRequest);
        $query->shouldHaveType(ListPostsQuery::class);
        $query->perPage()->shouldReturn(15);
        $query->page()->shouldReturn(2);
    }

    function it_should_transform_a_request_to_list_posts_query_when_parameters_are_invalid()
    {
        $psr7Factory = new DiactorosFactory();
        $symfonyRequest = Request::create(
            'https://insided.com?per_page=hello&page=bye',
            'GET'
        );
        $psrRequest = $psr7Factory->createRequest($symfonyRequest);

        $query = static::transform($psrRequest);
        $query->shouldHaveType(ListPostsQuery::class);
        $query->perPage()->shouldReturn(0);
        $query->page()->shouldReturn(0);
    }
}
