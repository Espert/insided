<?php

/*
 * This file is part of the insided/post bounded context.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace Insided\Post\Domain\Model\Repository;

use Insided\Common\Interaction\Response;
use Insided\Post\Domain\Model\Post;
use Insided\Post\Domain\Model\PostId;

/**
 * Domain Reader Repository: Post
 *
 * @author Fernando Coca <fcocajimeno@gmail.com>
 */
interface PostReaderRepository
{
    /**
     * @param \Insided\Post\Domain\Model\PostId $postId
     *
     * @return \Insided\Post\Domain\Model\Post|null
     */
    public function postOfId(PostId $postId): ?Post;

    /**
     * @param int $perPage
     * @param int $page
     *
     * @return \Insided\Common\Interaction\Response
     */
    public function allPosts(int $perPage, int $page = 1): Response;
}
