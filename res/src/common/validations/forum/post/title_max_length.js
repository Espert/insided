export default {
  getMessage() {
    return 'The post title must have 100 characters or less';
  },
  validate(val) {
    return val.length <= 100;
  },
};
