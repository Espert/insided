<?php

/*
 * This file is part of the insided/post bounded context.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace spec\Insided\Post\Application\Interaction\Transformer;

use Insided\Common\Interaction\Transformer;
use Insided\Post\Application\Interaction\Command\CreatePostCommand;
use Insided\Post\Application\Interaction\Transformer\CreatePostTransformer;
use Insided\Post\Domain\Model\Exception\PostMessageIsEmpty;
use Insided\Post\Domain\Model\Exception\PostTitleIsEmpty;
use Insided\Post\Domain\Model\PostMessage;
use Insided\Post\Domain\Model\PostTitle;
use PhpSpec\ObjectBehavior;
use Symfony\Bridge\PsrHttpMessage\Factory\DiactorosFactory;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Fernando Coca <fernando@creamplan.com>
 */
class CreatePostTransformerSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(CreatePostTransformer::class);
    }

    function it_is_a_message_transformer()
    {
        $this->shouldBeAnInstanceOf(Transformer::class);
    }

    function it_should_transform_a_request_to_create_post_command()
    {
        $psr7Factory = new DiactorosFactory();
        $symfonyRequest = Request::create(
            'https://insided.com',
            'POST',
            [
                'title' => 'post title',
                'message' => 'post message',
            ]
        );
        $psrRequest = $psr7Factory->createRequest($symfonyRequest);

        $command = static::transform($psrRequest);
        $command->shouldHaveType(CreatePostCommand::class);
        $command->title()->shouldHaveType(PostTitle::class);
        $command->title()->title()->shouldReturn('post title');
        $command->message()->shouldHaveType(PostMessage::class);
        $command->message()->message()->shouldReturn('post message');
    }

    function it_should_not_transform_a_request_to_create_post_comman_when_title_is_empty()
    {
        $psr7Factory = new DiactorosFactory();
        $symfonyRequest = Request::create(
            'https://insided.com',
            'POST',
            [
                'title' => '',
                'message' => 'post message',
            ]
        );
        $psrRequest = $psr7Factory->createRequest($symfonyRequest);

        $this
            ->shouldThrow(new PostTitleIsEmpty())
            ->during(
                'transform',
                [
                    $psrRequest,
                ]
            );
    }

    function it_should_not_transform_a_request_to_create_post_comman_when_title_is_not_provided()
    {
        $psr7Factory = new DiactorosFactory();
        $symfonyRequest = Request::create(
            'https://insided.com',
            'POST',
            [
                'message' => 'post message',
            ]
        );
        $psrRequest = $psr7Factory->createRequest($symfonyRequest);

        $this
            ->shouldThrow(new PostTitleIsEmpty())
            ->during(
                'transform',
                [
                    $psrRequest,
                ]
            );
    }

    function it_should_not_transform_a_request_to_create_post_comman_when_message_is_empty()
    {
        $psr7Factory = new DiactorosFactory();
        $symfonyRequest = Request::create(
            'https://insided.com',
            'POST',
            [
                'title' => 'post title',
                'message' => '',
            ]
        );
        $psrRequest = $psr7Factory->createRequest($symfonyRequest);

        $this
            ->shouldThrow(new PostMessageIsEmpty())
            ->during(
                'transform',
                [
                    $psrRequest,
                ]
            );
    }

    function it_should_not_transform_a_request_to_create_post_comman_when_message_is_not_provided()
    {
        $psr7Factory = new DiactorosFactory();
        $symfonyRequest = Request::create(
            'https://insided.com',
            'POST',
            [
                'title' => 'post title',
            ]
        );
        $psrRequest = $psr7Factory->createRequest($symfonyRequest);

        $this
            ->shouldThrow(new PostMessageIsEmpty())
            ->during(
                'transform',
                [
                    $psrRequest,
                ]
            );
    }
}
