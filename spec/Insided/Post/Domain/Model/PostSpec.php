<?php

/*
 * This file is part of the insided/post bounded context.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace spec\Insided\Post\Domain\Model;

use Insided\Common\Domain\Model\AggregateRoot;
use Insided\Post\Domain\Model\Post;
use Insided\Post\Domain\Model\PostId;
use Insided\Post\Domain\Model\PostMessage;
use Insided\Post\Domain\Model\PostTitle;
use PhpSpec\ObjectBehavior;

/**
 * @author Fernando Coca <fernando@creamplan.com>
 */
class PostSpec extends ObjectBehavior
{
    function let(PostId $postId, PostTitle $postTitle, PostMessage $postMessage)
    {
        $this->beConstructedThrough(
            'create',
            [
                $postId,
                $postTitle,
                $postMessage,
            ]
        );
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(Post::class);
    }

    function it_is_an_aggregate_root()
    {
        $this->shouldBeAnInstanceOf(AggregateRoot::class);
    }

    function it_should_have_an_identity()
    {
        $this->id()->shouldHaveType(PostId::class);
    }

    function it_should_have_a_creation_date()
    {
        $this->creationDate()->shouldHaveType(\DateTime::class);
    }

    function it_should_have_a_title()
    {
        $this->title()->shouldHaveType(PostTitle::class);
    }

    function it_should_have_a_message()
    {
        $this->message()->shouldHaveType(PostMessage::class);
    }
}
