<?php

/*
 * This file is part of the insided/post bounded context.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace Insided\Post\Domain\Model;

use Insided\Common\Domain\Model\ValueObject;
use Insided\Post\Domain\Model\Exception\PostTitleIsEmpty;
use Insided\Post\Domain\Model\Exception\PostTitleIsTooLong;

/**
 * Value Object: Post title
 *
 * @author Fernando Coca <fcocajimeno@gmail.com>
 */
class PostTitle implements ValueObject
{
    /**
     * @var string
     */
    protected $title;

    /**
     * @throws \Insided\Post\Domain\Model\Exception\PostTitleIsEmpty WHen title is an empty string.
     *
     * @param string $title
     */
    public function __construct(string $title)
    {
        $this->setTitle($title);
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @throws \Insided\Post\Domain\Model\Exception\PostTitleIsEmpty When title is an empty string.
     * @throws \Insided\Post\Domain\Model\Exception\PostTitleIsTooLong When title is longer than 100 chars.
     *
     * @return void
     */
    private function setTitle(string $title): void
    {
        if ($title === '') {
             throw new PostTitleIsEmpty();
        }

        if (\strlen($title) > 100) {
            throw new PostTitleIsTooLong();
        }

        $this->title = $title;
    }
}
