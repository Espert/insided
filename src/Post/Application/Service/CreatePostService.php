<?php

/*
 * This file is part of the insided/post bounded context.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace Insided\Post\Application\Service;

use Insided\Common\Application\TransactionalApplicationService;
use Insided\Common\Interaction\Message;
use Insided\Post\Application\Interaction\Command\CreatePostCommand;
use Insided\Post\Domain\Model\Post;
use Insided\Post\Domain\Model\Repository\PostWriterRepository;

/**
 * Application Service: Create Post
 *
 * @author Fernando Coca <fcocajimeno@gmail.com>
 */
class CreatePostService extends TransactionalApplicationService
{
    /**
     * @var \Insided\Post\Domain\Model\Repository\PostWriterRepository
     */
    private $postWriterRepository;

    /**
     * @param \Insided\Post\Domain\Model\Repository\PostWriterRepository $postWriterRepository
     */
    public function __construct(PostWriterRepository $postWriterRepository)
    {
        $this->postWriterRepository = $postWriterRepository;
        parent::__construct($postWriterRepository->manager());
    }

    /**
     * @param \Insided\Common\Interaction\Message|null $createPostCommand
     *
     * @throws \RuntimeException When the message provided is not a CreatePostCommand.
     * @throws \RuntimeException When the post could not be persisted.
     *
     * @return void
     */
    protected function executeWith(?Message $createPostCommand = null): void
    {
        if (! $createPostCommand instanceof CreatePostCommand) {
            throw new \RuntimeException(
                'A "CreatePostCommand" must be provided to execute "CreatePostService"'
            );
        }

        $post = Post::create(
            $createPostCommand->postId(),
            $createPostCommand->title(),
            $createPostCommand->message()
        );

        $this->postWriterRepository->add($post);
    }
}
