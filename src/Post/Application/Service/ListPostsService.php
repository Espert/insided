<?php

/*
 * This file is part of the insided/post bounded context.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace Insided\Post\Application\Service;

use Insided\Common\Application\ApplicationService;
use Insided\Common\Interaction\Message;
use Insided\Common\Interaction\Response;
use Insided\Post\Application\Interaction\Query\ListPostsQuery;
use Insided\Post\Application\Presentation\Assembler\PostAssembler;
use Insided\Post\Domain\Model\Repository\PostReaderRepository;

/**
 * Application Service: List Posts
 *
 * @author Fernando Coca <fcocajimeno@gmail.com>
 */
class ListPostsService extends ApplicationService
{
    /**
     * @var \Insided\Post\Domain\Model\Repository\PostReaderRepository
     */
    private $postReaderRepository;

    /**
     * @var \Insided\Post\Application\Presentation\Assembler\PostAssembler
     */
    private $postAssembler;

    /**
     * @param \Insided\Post\Domain\Model\Repository\PostReaderRepository $postReaderRepository
     */
    public function __construct(PostReaderRepository $postReaderRepository)
    {
        $this->postReaderRepository = $postReaderRepository;
        $this->postAssembler = new PostAssembler();
    }

    /**
     * @param \Insided\Common\Interaction\Message|null $listPostsQuery
     *
     * @throws \RuntimeException When the message provided is not a ListPostsQuery.
     *
     * @return \Insided\Common\Interaction\Response
     */
    protected function executeWith(?Message $listPostsQuery = null): Response
    {
        if (! $listPostsQuery instanceof ListPostsQuery) {
            throw new \RuntimeException(
                'A "ListPostsQuery" must be provided to execute "ListPostsService"'
            );
        }

        $postsResponse = $this
            ->postReaderRepository
            ->allPosts($listPostsQuery->perPage(), $listPostsQuery->page());

        return new Response(
            $this->assemblePosts($postsResponse->data()),
            $postsResponse->metadata()
        );
    }

    /**
     * @param \Insided\Post\Domain\Model\Post[] $posts
     *
     * @return \Insided\Post\Application\Presentation\Representation\PostRepresentation[]
     */
    private function assemblePosts(array $posts): array
    {
        $assembledPosts = [];

        foreach ($posts as $post) {
            $assembledPosts[] = $this->postAssembler->assemble($post);
        }

        return $assembledPosts;
    }
}
