<?php

/*
 * This file is part of the insided/post bounded context.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace Insided\Post\UserInterface\Action\Api;

use Insided\Common\Action\ApiAction;
use Insided\Post\Application\Interaction\Transformer\ListPostsTransformer;
use Insided\Post\Application\Presentation\Normalizer\PostNormalizer;
use Insided\Post\Application\Service\ListPostsService;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Api Action: List posts
 *
 * @author Fernando Coca <fcocajimeno@gmail.com>
 */
class ListPostsApiAction extends ApiAction
{
    /**
     * @var \Insided\Post\Application\Service\ListPostsService
     */
    private $listPostsService;

    /**
     * @param \Insided\Post\Application\Service\ListPostsService $listPostsService
     * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
     */
    public function __construct(
        ListPostsService $listPostsService,
        RequestStack $requestStack
    )
    {
        $this->listPostsService = $listPostsService;

        parent::__construct($requestStack);
    }

    /**
     * @throws \InvalidArgumentException When response could not be converted to JSON.
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function __invoke(): ResponseInterface
    {
        $request = $this->request();

        try {
            /** @var \Insided\Post\Application\Interaction\Query\ListPostsQuery $listPostQuery */
            $listPostQuery = ListPostsTransformer::transform($request);
            /** @var \Insided\Common\Interaction\Response $listPostsResponse */
            $listPostsResponse = $this->listPostsService->execute($listPostQuery);
        } catch (\Throwable $throwable) {
            return $this->jsonErrorResponse($throwable);
        }

        return $this->jsonResponse(
            [
                'posts' => $this->normalizePosts($listPostsResponse->data()),
            ],
            'ok',
            $listPostsResponse->metadata()
        );
    }

    /**
     * @param \Insided\Post\Application\Presentation\Representation\PostRepresentation[] $postRepresentations
     *
     * @return array
     */
    private function normalizePosts(array $postRepresentations): array
    {
        $normalizer = new PostNormalizer();
        $normalizedPosts = [];

        foreach ($postRepresentations as $postRepresentation) {
            $normalizedPosts[] = $normalizer->normalize($postRepresentation);
        }

        return $normalizedPosts;
    }
}
