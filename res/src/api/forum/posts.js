import Axios from 'axios';

const baseUri = process.env.API_URL;

export default {
  createPost(params, cb, errorcb) {
    const url = `${baseUri}/posts`;
    const request = new FormData();
    request.append('title', params.title);
    request.append('message', params.message);

    return Axios.post(url, request)
      .then(
        response => cb(response.data.data, response.data.status, response.data.meta),
        error => errorcb(error.response.data.status),
      );
  },
  listPosts(params, cb, errorcb) {
    const url = `${baseUri}/posts`;
    const request = {
      params: {
        per_page: params.perPage,
        page: params.page,
      },
    };

    return Axios.get(url, request)
      .then(
        response => cb(response.data.data, response.data.status, response.data.meta),
        error => errorcb(error.response.data.status),
      );
  },
};
