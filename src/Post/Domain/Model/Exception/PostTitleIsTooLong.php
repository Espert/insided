<?php

/*
 * This file is part of the insided/post bounded context.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace Insided\Post\Domain\Model\Exception;

/**
 * Domain Exception: Post title is too long
 *
 * @author Fernando Coca <fcocajimeno@gmail.com>
 */
class PostTitleIsTooLong extends \InvalidArgumentException
{
    public const MESSAGE = 'Post title must have 100 characters or less';

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct(self::MESSAGE);
    }
}
