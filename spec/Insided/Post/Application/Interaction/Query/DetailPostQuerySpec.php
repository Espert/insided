<?php

/*
 * This file is part of the insided/post bounded context.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace spec\Insided\Post\Application\Interaction\Query;

use Insided\Common\Interaction\Query;
use Insided\Post\Application\Interaction\Query\DetailPostQuery;
use Insided\Post\Domain\Model\PostId;
use PhpSpec\ObjectBehavior;

/**
 * @author Fernando Coca <fernando@creamplan.com>
 */
class DetailPostQuerySpec extends ObjectBehavior
{
    function let(PostId $postId)
    {
        $this->beConstructedWith($postId);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(DetailPostQuery::class);
    }

    function it_is_a_query()
    {
        $this->shouldBeAnInstanceOf(Query::class);
    }

    function it_should_have_a_post_id()
    {
        $this->postId()->shouldHaveType(PostId::class);
    }
}
