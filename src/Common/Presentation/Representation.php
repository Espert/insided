<?php

/*
 * This file is part of the insided/common package.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace Insided\Common\Presentation;

/**
 * Base interface for representations
 *
 * @author Fernando Coca <fcocajimeno@gmail.com>
 */
interface Representation
{
}
