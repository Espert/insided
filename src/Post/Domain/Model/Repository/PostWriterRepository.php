<?php

/*
 * This file is part of the insided/post bounded context.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace Insided\Post\Domain\Model\Repository;

use Insided\Post\Domain\Model\Post;

/**
 * Domain Writer Repository: Post
 *
 * @author Fernando Coca <fcocajimeno@gmail.com>
 */
interface PostWriterRepository
{
    /**
     * @param \Insided\Post\Domain\Model\Post $post
     *
     * @return void
     */
    public function add(Post $post): void;

    /**
     * @param \Insided\Post\Domain\Model\Post $post
     *
     * @return void
     */
    public function remove(Post $post): void;

    /**
     * @return mixed
     */
    public function manager();
}
