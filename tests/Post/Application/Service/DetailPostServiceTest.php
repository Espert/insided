<?php

/*
 * This file is part of the insided/post bounded context.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace Insided\Post\Application\Service;

use Insided\Common\Domain\IdentityGenerator;
use Insided\Common\Testing\SymfonyDoctrine\PHPUnitSymfonyDoctrineTestCase;
use Insided\Post\Application\Interaction\Query\DetailPostQuery;
use Insided\Post\Application\Presentation\Representation\PostRepresentation;
use Insided\Post\Domain\Model\PostId;
use Insided\Post\Infrastructure\Persistence\Doctrine\Fixture\PostDataFixture;

/**
 * @author Fernando Coca <fcocajimeno@gmail.com>
 */
class DetailPostServiceTest extends PHPUnitSymfonyDoctrineTestCase
{
    /**
     * @return string
     */
    protected function managerName(): string
    {
        return 'insided';
    }

    /**
     * @return array
     */
    protected function fixtures(): array
    {
        return [
            PostDataFixture::class,
        ];
    }

    public function test_it_retrieves_a_post_by_id_when_it_exists()
    {
        $postRepresentation  =
            $this->detailPostService()->execute(
                new DetailPostQuery(
                    new PostId('a5284134-d1ab-4a99-90d5-c30f8b3fb023')
                )
            );

        self::assertInstanceOf(PostRepresentation::class, $postRepresentation);
        self::assertSame('a5284134-d1ab-4a99-90d5-c30f8b3fb023', $postRepresentation->id());
    }

    public function test_it_not_retrieves_a_post_by_id_when_it_does_not_exists()
    {
        $postRepresentation  =
            $this->detailPostService()->execute(
                new DetailPostQuery(
                    new PostId(IdentityGenerator::generate())
                )
            );

        self::assertNull($postRepresentation);
    }

    private function detailPostService(): DetailPostService
    {
        return static::service('insided.post.app_service.detail_post');
    }
}
