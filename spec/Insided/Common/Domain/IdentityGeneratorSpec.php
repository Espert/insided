<?php

/*
 * This file is part of the insided/common package.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace spec\Insided\Common\Domain;

use Insided\Common\Domain\IdentityGenerator;
use PhpSpec\ObjectBehavior;
use Assert\Assertion as Assert;

/**
 * @author Fernando Coca <fernando@creamplan.com>
 */
class IdentityGeneratorSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(IdentityGenerator::class);
    }

    function it_should_generate_an_unique_uuid()
    {
        $uuid = IdentityGenerator::generate();

        Assert::uuid($uuid);
    }
}
