<?php

/*
 * This file is part of the insided/post bounded context.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace spec\Insided\Post\Application\Interaction\Command;

use Insided\Common\Interaction\Command;
use Insided\Post\Application\Interaction\Command\CreatePostCommand;
use Insided\Post\Domain\Model\PostId;
use Insided\Post\Domain\Model\PostMessage;
use Insided\Post\Domain\Model\PostTitle;
use PhpSpec\ObjectBehavior;

/**
 * @author Fernando Coca <fernando@creamplan.com>
 */
class CreatePostCommandSpec extends ObjectBehavior
{
    function let(PostId $postId, PostTitle $postTitle, PostMessage $postMessage)
    {
        $this->beConstructedWith($postId, $postTitle, $postMessage);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(CreatePostCommand::class);
    }

    function it_is_a_command()
    {
        $this->shouldBeAnInstanceOf(Command::class);
    }

    function it_should_have_a_post_id()
    {
        $this->postId()->shouldHaveType(PostId::class);
    }

    function it_should_have_a_message()
    {
        $this->message()->shouldHaveType(PostMessage::class);
    }

    function it_should_have_a_title()
    {
        $this->title()->shouldHaveType(PostTitle::class);
    }
}
