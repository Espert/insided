import * as types from '../../mutation-types';
import postApi from '../../../api/forum/posts';

// initial state
/* eslint no-shadow: 0, no-param-reassign: 0*/
const state = {
  posts: [],
  meta: {
    total: 0,
    showing: 3,
    page: 1,
    pages: 1,
  },
  status: {
    code: 2000,
    text: 'ok',
    type: 'success',
  },
};

// getters
const getters = {};

// actions
const actions = {
  createPost({ commit }, { params, cb, cberror }) {
    return postApi.createPost(
      params,
      (data, status, meta) => {
        commit(types.SHOW_MESSAGE, { message: status.text });
        commit(types.POST_CREATED, { data, status, meta });
        cb(data, status, meta);
      },
      (status) => {
        commit(types.SHOW_ERROR_MESSAGE, { message: status.text });
        cberror(status);
      },
    );
  },
  listPosts({ commit }, { params, cb, cberror }) {
    return postApi.listPosts(
      params,
      (data, status, meta) => {
        commit(types.POSTS_LISTED, { data, status, meta });
        cb(data, status, meta);
      },
      (status) => {
        commit(types.SHOW_ERROR_MESSAGE, { message: status.text });
        cberror(status);
      },
    );
  },
};

// mutations
const mutations = {
  [types.POST_CREATED](state, { data }) {
    state.posts.push(data.posts[0]);
  },
  [types.POSTS_LISTED](state, { data, status, meta }) {
    if (meta.page > 1) {
      state.posts = state.posts.concat(data.posts);
    } else {
      state.posts = data.posts;
    }
    state.meta = meta;
    state.status = status;
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
