<?php

/*
 * This file is part of the insided/post bounded context.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace Insided\Post\Application\Interaction\Command;

use Insided\Common\Interaction\Command;
use Insided\Post\Domain\Model\PostId;
use Insided\Post\Domain\Model\PostMessage;
use Insided\Post\Domain\Model\PostTitle;

/**
 * Command: Create Post
 *
 * @author Fernando Coca <fcocajimeno@gmail.com>
 */
class CreatePostCommand implements Command
{
    /**
     * @var \Insided\Post\Domain\Model\PostTitle
     */
    protected $title;

    /**
     * @var \Insided\Post\Domain\Model\PostMessage
     */
    protected $message;

    /**
     * @var \Insided\Post\Domain\Model\PostId
     */
    protected $postId;

    /**
     * @param \Insided\Post\Domain\Model\PostId $postId
     * @param \Insided\Post\Domain\Model\PostTitle $title
     * @param \Insided\Post\Domain\Model\PostMessage $message
     */
    public function __construct(
        PostId $postId,
        PostTitle $title,
        PostMessage $message
    )
    {
        $this->postId = $postId;
        $this->title = $title;
        $this->message = $message;
    }

    /**
     * @return string
     */
    public static function successMessage(): string
    {
        return 'The post was created successfully';
    }

    /**
     * @return \Insided\Post\Domain\Model\PostTitle
     */
    public function title(): PostTitle
    {
        return $this->title;
    }

    /**
     * @return \Insided\Post\Domain\Model\PostMessage
     */
    public function message(): PostMessage
    {
        return $this->message;
    }

    /**
     * @return \Insided\Post\Domain\Model\PostId
     */
    public function postId(): PostId
    {
        return $this->postId;
    }
}
