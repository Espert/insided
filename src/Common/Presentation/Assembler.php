<?php

/*
 * This file is part of the insided/common package.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace Insided\Common\Presentation;

use Insided\Common\Domain\Model\AggregateRoot;

/**
 * Base interface for assemblers
 *
 * @author Fernando Coca <fcocajimeno@gmail.com>
 */
interface Assembler
{
    /**
     * @param \Insided\Common\Domain\Model\AggregateRoot $aggregateRoot
     *
     * @return \Insided\Common\Presentation\Representation\null
     */
    public function assemble(AggregateRoot $aggregateRoot): ?Representation;
}
