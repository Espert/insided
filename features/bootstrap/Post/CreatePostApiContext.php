<?php

namespace Post;

use Insided\Common\Testing\Behat\ApiContext;
use FeatureContext;
use Assert\Assertion as Assert;

/**
 * This context class contains the definitions of the steps used by the demo
 * feature file. Learn how to get started with Behat and BDD on Behat's website.
 *
 * @see http://behat.org/en/latest/quick_start.html
 */
class CreatePostApiContext extends FeatureContext
{
    use ApiContext;

    private $createPostUrl = 'insided_post_action_api_create_post';

    /**
     * @Given I am creating a post with title :title and message :message
     */
    public function iAmCreatingAPostWithTitleAndMessage($title, $message)
    {
        $this->addFormParams([
            'title' => $title === "''" ? '' : $title,
            'message' => $message === "''" ? '' : $message,
        ]);
    }

    /**
     * @When I create the post
     */
    public function iCreateThePost()
    {
        $this->send($this->getUrl([], $this->createPostUrl), 'POST');
    }

    /**
     * @Then I should see a new post with title :title and message :message
     */
    public function iShouldSeeANewPostWithTitlePostTitleAndMessage($title, $message)
    {
        $profiles = $this->parseJson($this->response->getBody());
        Assert::same($profiles['data']['posts'][0]['title'], $title);
        Assert::same($profiles['data']['posts'][0]['message'], $message);
    }
}
