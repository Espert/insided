// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import Vuex from 'vuex';
import VueRouter from 'vue-router';
import Moment from 'moment';
import { extendMoment } from 'moment-range';
import { sync } from 'vuex-router-sync';
import { Validator } from 'vee-validate';
import App from './spa/App';
import plugins from './config/plugins';
import configRouter from './config/router';
import store from './store/store';
import validations from './config/validations';

extendMoment(Moment);
Vue.config.productionTip = false;

plugins(Vue, Vuex, VueRouter);
validations(Validator);
const router = configRouter(VueRouter);

sync(store, router);

/* eslint-disable */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App },
});
