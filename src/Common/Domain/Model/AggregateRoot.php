<?php

/*
 * This file is part of the insided/common package.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace Insided\Common\Domain\Model;

/**
 * Base class for aggregate roots
 *
 * @author Fernando Coca <fcocajimeno@gmail.com>
 */
class AggregateRoot
{
    /**
     * @var \Insided\Common\Domain\Model\Identity|null
     */
    protected $id;

    /**
     * @return \Insided\Common\Domain\Model\Identity|null
     */
    public function id(): ?Identity
    {
        return $this->id;
    }

    /**
     * @param \Insided\Common\Domain\Model\Identity $identity
     *
     * @return void
     */
    protected function setId(Identity $identity): void
    {
        $this->id = $identity;
    }
}
