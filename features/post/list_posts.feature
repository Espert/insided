@post
@list_posts
Feature: List posts
  In order to view all published posts
  As an user
  I want to be able to list and filter posts

  @api
  Scenario: Listing posts
    Given I am listing posts with per page "6" and page "1"
    When I list posts
    Then I should see message "ok"
    And I should see a post with title "Ni siquiera los todopoderosos signos de puntuación dominan" in position "0"
    And I should see a post with title "Sed ut perspiciatis unde omnis iste natus error si" in position "1"
    And I should see a post with title "Li Europan lingues es membres del sam familie. Lor separat existentie es un myth" in position "2"
    And I should see a post with title "Lorem ipsum dolor sit amet, consectetuer adipiscin" in position "3"
    And I should see a post with title "¿Qué me ha ocurrido? No estaba soñando" in position "4"
    And I should see a post with title "habiendo durado mucho el mal, el bien está ya cerca" in position "5"

  @api
  Scenario: Listing posts with pagination
    Given I am listing posts with per page "3" and page "1"
    When I list posts
    Then I should see message "ok"
    And I should see a post with title "Ni siquiera los todopoderosos signos de puntuación dominan" in position "0"
    And I should see a post with title "Sed ut perspiciatis unde omnis iste natus error si" in position "1"
    And I should see a post with title "Li Europan lingues es membres del sam familie. Lor separat existentie es un myth" in position "2"
    Given I am listing posts with per page "3" and page "2"
    When I list posts
    Then I should see message "ok"
    And I should see a post with title "Lorem ipsum dolor sit amet, consectetuer adipiscin" in position "0"
    And I should see a post with title "¿Qué me ha ocurrido? No estaba soñando" in position "1"
    And I should see a post with title "habiendo durado mucho el mal, el bien está ya cerca" in position "2"
