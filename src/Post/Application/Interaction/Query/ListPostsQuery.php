<?php

/*
 * This file is part of the insided/post bounded context.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace Insided\Post\Application\Interaction\Query;

use Insided\Common\Interaction\Query;

/**
 * Query: List and filter posts
 *
 * @author Fernando Coca <fcocajimeno@gmail.com>
 */
class ListPostsQuery implements Query
{
    /**
     * @var int
     */
    protected $perPage;

    /**
     * @var int
     */
    protected $page;

    /**
     * @param int $perPage
     * @param int $page
     */
    public function __construct(int $perPage = 3, int $page = 1)
    {
        $this->perPage = $perPage;
        $this->page = $page;
    }

    /**
     * @return string
     */
    public static function errorMessage(): string
    {
        return 'It was not possible to retrieve posts list';
    }

    /**
     * @return int
     */
    public function page(): int
    {
        return $this->page;
    }

    /**
     * @return int
     */
    public function perPage(): int
    {
        return $this->perPage;
    }
}
