<?php

/*
 * This file is part of the insided/common package.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace Insided\Common\Testing\Behat;

use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Behat\Behat\Context\BehatContext;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\SchemaTool;

/**
 * Provides methods that can recursively load fixtures that implement DependentFixtureInterface
 */
trait DoctrineFixturesContext
{
    /**
     * Load a data fixture class
     *
     * @param \Doctrine\Common\DataFixtures\Loader $loader    Data fixtures loader
     * @param string                               $className Class name of fixture
     */
    public function loadFixtureClass($className, $loader = null)
    {
        $notLoader = false;
        if (null === $loader) {
            $loader = $this->getLoader();
            $notLoader = true;
        }

        $fixture = new $className();
        if ($loader->hasFixture($fixture)) {
            unset($fixture);
            return;
        }
        $loader->addFixture(new $className);
        if ($fixture instanceof DependentFixtureInterface) {
            foreach ($fixture->getDependencies() as $dependency) {
                $this->loadFixtureClass($loader, $dependency);
            }
        }

        if ($notLoader) {
            $this->load($loader);
        }
    }

    /**
     * Load a data fixture class
     *
     * @param \Doctrine\Common\DataFixtures\Loader $loader     Data fixtures loader
     * @param array                                $classNames Array of class names of fixtures
     */
    public function loadFixtureClasses(array $classNames, $purge = false)
    {
        $managers = $this->getContainer()->get('doctrine')->getManagers();

        if (count($classNames) > 0){

            $loader = $this->getLoader();
            foreach ($classNames as $className) {
                $this->loadFixtureClass($className, $loader);
            }

            $this->load($loader, true);
        }
    }

    private function getLoader()
    {
        return new Loader();
    }

    private function load($loader, $purge = false)
    {
        $registry = $this->managerRegistry();

        //if (null !== $registry) {
        //    $em = $registry->get();
        //} else {
        /** @var $em \Doctrine\ORM\EntityManager */
        if (null !== $this->getContainer()) {
            $em = $this->getContainer()->get('doctrine')->getManager();
        } else {
            $em = $this->kernel->getContainer()->get('doctrine')->getManager();
        }

        //}
        //$em->getConfiguration()->setAutoGenerateProxyClasses(1);
        //$em->getConfiguration()->setProxyDir(\sys_get_temp_dir());
        $purger = new ORMPurger();
        $purger->setPurgeMode(ORMPurger::PURGE_MODE_TRUNCATE);
        $executor = new ORMExecutor($em, $purger);
        if ($purge) {
            $executor->purge();
        }

        $executor->execute($loader->getFixtures(), true);
    }

    /**
     * @param \Behat\Event\ScenarioEvent|\Behat\Behat\Event\OutlineExampleEvent $event
     *
     * @return null
     */
    public function buildSchema(string $entityManagerName = '')
    {
        $this->closeDBALConnections();

        foreach ($this->getEntityManagers() as $entityManager) {

            $metadata = $this->getMetadata($entityManager);
            if (!empty($metadata)) {

                try{

                    $tool = new SchemaTool($entityManager);
                    $tool->dropSchema($metadata);
                    $tool->createSchema($metadata);
                } catch(\Exception $e) {

                }
            }
        }

    }
    /**
     * @param \Behat\Behat\Event\ScenarioEvent|\Behat\Behat\Event\OutlineExampleEvent $event
     *
     * @AfterScenario
     *
     * @return null
     */
    public function closeDBALConnections()
    {
        /** @var EntityManager $entityManager */
        foreach ($this->getEntityManagers() as $entityManager) {
            $entityManager->clear();
        }
        /** @var Connection $connection */
        foreach ($this->getConnections() as $connection) {
            $connection->close();
        }
    }

    /**
     * @return array
     */
    protected function getEntityManager(string $entityManagerName = null)
    {
        return null !== $this->getContainer()
            ? $this->getContainer()->get('doctrine')->getManager($entityManagerName)
            : $this->kernel->getContainer()->get('doctrine')->getManager($entityManagerName);
    }

    /**
     * @param EntityManager $entityManager
     *
     * @return array
     */
    protected function getMetadata(EntityManager $entityManager)
    {
        return $entityManager->getMetadataFactory()->getAllMetadata();
    }
    /**
     * @return EntityManager[]
     */
    protected function getEntityManagers()
    {
        return $this->getContainer()->get('doctrine')->getManagers();
    }
    /**
     * @return mixed
     */
    protected function managerRegistry()
    {
        return null;
    }
    /**
     * @return array
     */
    protected function getConnections()
    {
        return null !== $this->getContainer()
            ? $this->getContainer()->get('doctrine')->getConnections()
            : $this->kernel->getContainer()->get('doctrine')->getConnections();
    }
}
