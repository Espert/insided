<?php

/*
 * This file is part of the insided/common package.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace Insided\Common\Presentation;

/**
 * Base interface for normalizers
 *
 * @author Fernando Coca <fcocajimeno@gmail.com>
 */
interface Normalizer
{
    /**
     * Transforms a representation to a plain array
     *
     * @param \Insided\Common\Presentation\Representation $representation
     *
     * @return array|null
     */
    public function normalize(Representation $representation): ?array;
}
