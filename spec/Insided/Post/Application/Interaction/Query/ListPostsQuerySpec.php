<?php

/*
 * This file is part of the insided/post bounded context.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace spec\Insided\Post\Application\Interaction\Query;

use Insided\Common\Interaction\Query;
use Insided\Post\Application\Interaction\Query\ListPostsQuery;
use PhpSpec\ObjectBehavior;

/**
 * @author Fernando Coca <fernando@creamplan.com>
 */
class ListPostsQuerySpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(ListPostsQuery::class);
    }

    function it_is_a_query()
    {
        $this->shouldBeAnInstanceOf(Query::class);
    }

    function it_should_have_a_per_page()
    {
        $this->perPage()->shouldReturn(3);
    }

    function it_should_have_a_page()
    {
        $this->page()->shouldReturn(1);
    }

    function it_could_be_created_with_page()
    {
        $this->beConstructedWith(3, 10);
        $this->page()->shouldReturn(10);
    }

    function it_could_be_created_with_per_page()
    {
        $this->beConstructedWith(5);
        $this->perPage()->shouldReturn(5);
    }
}
