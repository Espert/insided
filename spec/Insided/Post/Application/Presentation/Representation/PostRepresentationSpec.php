<?php

/*
 * This file is part of the insided/post bounded context.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace spec\Insided\Post\Application\Presentation\Representation;

use Insided\Common\Presentation\Representation;
use Insided\Post\Application\Presentation\Representation\PostRepresentation;
use PhpSpec\ObjectBehavior;

/**
 * @author Fernando Coca <fernando@creamplan.com>
 */
class PostRepresentationSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith(
            'a5534de8-0f40-4cad-8912-38302cb5f08b',
            'post title',
            'post message',
            '2017-06-30T10:00:00'
        );
    }
    function it_is_initializable()
    {
        $this->shouldHaveType(PostRepresentation::class);
    }

    function it_is_a_representation()
    {
        $this->shouldBeAnInstanceOf(Representation::class);
    }

    function it_should_have_an_id()
    {
        $this->id()->shouldReturn('a5534de8-0f40-4cad-8912-38302cb5f08b');
    }

    function it_should_have_a_title()
    {
        $this->title()->shouldReturn('post title');
    }

    function it_should_have_a_message()
    {
        $this->message()->shouldReturn('post message');
    }

    function it_should_have_a_created_on_date()
    {
        $this->createdOn()->shouldReturn('2017-06-30T10:00:00');
    }
}
