<?php

/*
 * This file is part of the insided/common package.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace Insided\Common\Interaction;

use Psr\Http\Message\ServerRequestInterface;

/**
 * Base interface for message transformers
 *
 * @author Fernando Coca <fcocajimeno@gmail.com>
 */
interface Transformer
{
    /**
     * @param \Psr\Http\Message\ServerRequestInterface $request
     *
     * @return \Insided\Common\Interaction\Message
     */
    public static function transform(ServerRequestInterface $request): Message;
}
