import Vue from 'vue';
import Vuex from 'vuex';
import createLogger from '../common/plugins/logger';
import app from './modules/application';
import posts from './modules/forum/posts';

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';


export default new Vuex.Store({
  modules: {
    app,
    posts,
  },
  strict: debug,
  plugins: debug ? [createLogger()] : [],
});
