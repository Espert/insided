<?php

/*
 * This file is part of the insided/common package.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace spec\Insided\Common\Domain\Model;

use Insided\Common\Domain\Model\AggregateRoot;
use PhpSpec\ObjectBehavior;

/**
 * @author Fernando Coca <fernando@creamplan.com>
 */
class AggregateRootSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(AggregateRoot::class);
    }

    function it_should_not_have_an_id_bty_default()
    {
        $this->id()->shouldBe(null);
    }
}
