<?php

/*
 * This file is part of the insided/post bounded context.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace Insided\Post\Application\Service;

use Insided\Common\Testing\SymfonyDoctrine\PHPUnitSymfonyDoctrineTestCase;
use Insided\Post\Application\Interaction\Query\ListPostsQuery;
use Insided\Post\Application\Presentation\Representation\PostRepresentation;
use Insided\Post\Infrastructure\Persistence\Doctrine\Fixture\PostDataFixture;

/**
 * @author Fernando Coca <fcocajimeno@gmail.com>
 */
class ListPostsServiceTest extends PHPUnitSymfonyDoctrineTestCase
{
    /**
     * @return string
     */
    protected function managerName(): string
    {
        return 'insided';
    }

    /**
     * @return array
     */
    protected function fixtures(): array
    {
        return [
            PostDataFixture::class,
        ];
    }

    public function test_it_retrieves_a_list_of_posts_ordered_by_creation()
    {
        $postsResponse  =
            $this->listPostService()->execute(
                new ListPostsQuery(6)
            );

        self::assertContainsOnlyInstancesOf(PostRepresentation::class, $postsResponse->data());
        self::assertCount(6, $postsResponse->data());
        self::assertSame(6, $postsResponse->metadata()['total']);
        self::assertSame(1, $postsResponse->metadata()['page']);
        self::assertSame(1, $postsResponse->metadata()['pages']);
        self::assertSame(6, $postsResponse->metadata()['showing']);

        $actualDate = null;

        foreach ($postsResponse->data() as $post) {
            if ($actualDate === null) {
                $actualDate = \DateTime::createFromFormat('Y-m-d\TH:i:s', $post->createdOn());
            } else {
                self::assertTrue($actualDate > \DateTime::createFromFormat('Y-m-d\TH:i:s', $post->createdOn()));
            }
        }
    }

    private function listPostService(): ListPostsService
    {
        return static::service('insided.post.app_service.list_posts');
    }
}
