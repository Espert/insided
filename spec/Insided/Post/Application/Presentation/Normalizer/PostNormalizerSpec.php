<?php

/*
 * This file is part of the insided/post bounded context.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace spec\Insided\Post\Application\Presentation\Normalizer;

use Insided\Common\Presentation\Normalizer;
use Insided\Common\Presentation\Representation;
use Insided\Post\Application\Presentation\Normalizer\PostNormalizer;
use Insided\Post\Application\Presentation\Representation\PostRepresentation;
use PhpSpec\ObjectBehavior;

/**
 * @author Fernando Coca <fernando@creamplan.com>
 */
class PostNormalizerSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(PostNormalizer::class);
    }

    function it_is_a_normalizer()
    {
        $this->shouldBeAnInstanceOf(Normalizer::class);
    }

    function it_should_normalize_only_post_representations(Representation $representation)
    {
        $this->normalize($representation)->shouldReturn(null);
    }

    function it_should_normalize_a_post()
    {
        $postRepresentation = new PostRepresentation(
            'a5534de8-0f40-4cad-8912-38302cb5f08b',
            'post title',
            'post message',
            '2017-06-30T10:00:00'
        );

        $this->normalize($postRepresentation)->shouldReturn([
            'id' => 'a5534de8-0f40-4cad-8912-38302cb5f08b',
            'title' => 'post title',
            'message' => 'post message',
            'createdOn' => '2017-06-30T10:00:00',
        ]);
    }
}
