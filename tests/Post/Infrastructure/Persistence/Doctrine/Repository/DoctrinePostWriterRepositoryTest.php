<?php

/*
 * This file is part of the insided/post bounded context.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace Insided\Post\Infrastructure\Persistence\Doctrine\Repository;

use Insided\Common\Domain\IdentityGenerator;
use Insided\Common\Testing\SymfonyDoctrine\PHPUnitSymfonyDoctrineTestCase;
use Insided\Post\Domain\Model\Post;
use Insided\Post\Domain\Model\PostId;
use Insided\Post\Domain\Model\PostMessage;
use Insided\Post\Domain\Model\PostTitle;
use Insided\Post\Domain\Model\Repository\PostReaderRepository;
use Insided\Post\Domain\Model\Repository\PostWriterRepository;

/**
 * @author Fernando Coca <fcocajimeno@gmail.com>
 */
class DoctrinePostWriterRepositoryTest extends PHPUnitSymfonyDoctrineTestCase
{
    /**
     * @return string
     */
    protected function managerName(): string
    {
        return 'insided';
    }

    /**
     * @return array
     */
    protected function fixtures(): array
    {
        return [];
    }

    public function test_it_should_add_a_post()
    {
        $postId = new PostId(IdentityGenerator::generate());
        $post = Post::create(
            $postId,
            new PostTitle('post title'),
            new PostMessage('post message')
        );

        $this->postWriterRepository()->add($post);
        $this->postWriterRepository()->manager()->flush();

        $postAdded = $this->postReaderRepository()->postOfId($postId);

        self::assertNotNull($postAdded);
        self::assertSame($postAdded->id()->id(), $postId->id());
    }

    private function postWriterRepository(): PostWriterRepository
    {
        return static::service('insided.post.repository.writer.post');
    }

    private function postReaderRepository(): PostReaderRepository
    {
        return static::service('insided.post.repository.reader.post');
    }
}
