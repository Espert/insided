# inSided Forum #

Assignment for a PHP/FE Engineer

Version 1.0

## Demo ##

* [inSided Forum](http://ec2-52-59-42-105.eu-central-1.compute.amazonaws.com/posts)

## Application ##

### Front ###

* GET /posts (Posts list)
* GET /posts/creation (Creation form)
    
### API ###

* POST /api/v1/posts (Creation)

    Parameters:
    - title: string
    - message: text
    
* GET /api/v1/posts (List)
                   
    Query params:
    - per_page: int
    - page: int

## Development ##

### Requirements ###

* [Docker](https://docs.docker.com/engine/installation)
* [Docker Compose](https://docs.docker.com/compose/install)
* PHP
* [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
* [Composer](https://getcomposer.org/download)
* [Yarn](https://yarnpkg.com/lang/en/docs/install/)

### Installation ###

Clone git repository:

>    $ git clone git@bitbucket.org:Espert/insided.git insided

Install dependencies with composer:

>    $ cd insided && composer install

Install front dependencies with yarn:

>    $ cd res && yarn install && cd ..

Create symlinks for static assets:

>    $ cd web && mkdir -p static && cd static 
        && ln -s ../../res/static/img/ img 
        && ln -s ../../res/dist/ dist && cd ../..

### Run application ###

Run dev infrastructure with docker compose:

>    $ sudo docker-compose -f docker-compose.dev.yml up -d

Run nodejs dev server with yarn:

>    $ cd res && yarn run dev

Create database schema:

>    $ sudo docker exec -ti insided_php-fpm_1 bash -c "cd /var/www/html && php bin/console doctrine:schema:create"


### Stop application ###

>    $ sudo docker-compose -f docker-compose.dev.yml stop

>    $ sudo docker-compose -f docker-compose.dev.yml rm --force

### Testing ###

Create test database:

>    $ sudo docker exec -ti insided_postgres_1 psql --username=postgres -c 'CREATE DATABASE postgres_test;'

End to end tests:

>    $ vendor/bin/behat

Integration tests:

>    $ vendor/bin/phpunit

Unit tests:

>    $ vendor/bin/phpspec run