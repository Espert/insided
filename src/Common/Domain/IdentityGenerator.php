<?php

/*
 * This file is part of the insided/common package.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace Insided\Common\Domain;

use Ramsey\Uuid\Uuid;

/**
 * String based uuid generator
 *
 * @author Fernando Coca <fcocajimeno@gmail.com>
 */
final class IdentityGenerator
{
    /**
     * Returns a new unique uuid
     *
     * @return string
     */
    public static function generate(): string
    {
        return Uuid::uuid4()->toString();
    }
}
