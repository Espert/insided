<?php

/*
 * This file is part of the insided/forum bounded context.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace Insided\Forum\UserInterface\Action\View;

use Twig\Environment;
use Symfony\Component\HttpFoundation\Response;

/**
 * View Action: Forum
 *
 * @author Fernando Coca <fcocajimeno@gmail.com>
 */
class ForumViewAction
{
    /**
     * @var string
     */
    private $manifestFilePath;

    /**
     * @var string
     */
    private $basePath;

    /**
     * @var string
     */
    private $staticPath;

    /**
     * @var string
     */
    private $environment;

    /**
     * @var string
     */
    private $template;

    /**
     * @var \Twig\Environment
     */
    private $templating;

    /**
     * @param string $basePath
     * @param string $environment
     * @param string $template
     * @param \Twig\Environment $templating
     */
    public function __construct(
        string $basePath,
        string $environment,
        string $template,
        Environment $templating
    )
    {
        $this->manifestFilePath = '';
        $this->environment = $environment;
        $this->basePath = $basePath;
        $this->templating = $templating;
        $this->template = $template;
        $this->staticPath = '/static/dist/';

        if ($environment === 'prod') {
            $this->manifestFilePath = "${basePath}/res/dist/manifest.json";
        }
    }

    /**
     * @return mixed
     */
    public function __invoke()
    {
        $manifest = [];
        $isProd = $this->environment === 'prod';
        $manifestExists = $this->manifestFilePath  !== ''&& \file_exists($this->manifestFilePath);

        if ($isProd && $manifestExists) {
            try {
                $manifest = \json_decode(\file_get_contents($this->manifestFilePath), true);
            } catch (\Throwable $e) {
            }
        }

        return new Response(
            $this->templating->render(
                $this->template,
                [
                    'appCss' => $this->staticPath.(array_key_exists('app.css', $manifest) ? $manifest['app.css'] : ''),
                    'appJs' => $this->staticPath.(array_key_exists('app.js', $manifest) ? $manifest['app.js'] : ''),
                    'vendorJs' => $this->staticPath.(array_key_exists('vendor.js', $manifest) ? $manifest['vendor.js'] : ''),
                    'manifestJs' => $this->staticPath.(array_key_exists('manifest.js', $manifest) ? $manifest['manifest.js'] : ''),
                ]
            )
        );
    }
}
