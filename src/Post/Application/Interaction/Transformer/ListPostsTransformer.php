<?php

/*
 * This file is part of the insided/post bounded context.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace Insided\Post\Application\Interaction\Transformer;

use Insided\Common\Interaction\Message;
use Insided\Common\Interaction\Transformer;
use Insided\Post\Application\Interaction\Query\ListPostsQuery;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Transformer: List posts
 *
 * @author Fernando Coca <fcocajimeno@gmail.com>
 */
class ListPostsTransformer implements Transformer
{
    /**
     * @param \Psr\Http\Message\ServerRequestInterface $request
     *
     * @return \Insided\Common\Interaction\Message
     */
    public static function transform(ServerRequestInterface $request): Message
    {
        $params = $request->getQueryParams();

        $perPage = \array_key_exists('per_page', $params)
            ? (int) $params['per_page']
            : 3;

        $page = \array_key_exists('page', $params)
            ? (int) $params['page']
            : 1;

        return new ListPostsQuery($perPage, $page);
    }
}
