<?php

/*
 * This file is part of the insided/post bounded context.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace Insided\Post\Infrastructure\Persistence\Doctrine\Repository;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Insided\Common\Interaction\Response;
use Insided\Post\Domain\Model\Post;
use Insided\Post\Domain\Model\PostId;
use Insided\Post\Domain\Model\Repository\PostReaderRepository;

/**
 * Domain Reader Repository: Post
 *
 * @author Fernando Coca <fcocajimeno@gmail.com>
 */
class DoctrinePostReaderRepository implements PostReaderRepository
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @param \Doctrine\ORM\EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param \Insided\Post\Domain\Model\PostId $postId
     *
     * @return \Insided\Post\Domain\Model\Post|null
     */
    public function postOfId(PostId $postId): ?Post
    {
        $qb = $this->entityManager->createQueryBuilder();

        try {
            return $qb
                ->select('post')
                ->from(Post::class, 'post')
                ->where($qb->expr()->eq('post.id.id', ':postId'))
                ->setParameter('postId', $postId->id())
                ->getQuery()
                ->getOneOrNullResult();
        } catch (\Doctrine\ORM\NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * @param int $perPage
     * @param int $page
     *
     * @return \Insided\Common\Interaction\Response
     */
    public function allPosts(int $perPage, int $page = 1): Response
    {
        $qb = $this->entityManager->createQueryBuilder();
        $normalizedPage = $page - 1;
        $firstResult = $perPage * $normalizedPage;
        try {
            $query = $qb
                ->select('post')
                ->from(Post::class, 'post')
                ->setFirstResult($firstResult)
                ->setMaxResults($perPage)
                ->orderBy('post.creationDate', 'DESC')
                ->getQuery();

            $paginatorResult = new Paginator($query);
            $countTotal = $paginatorResult->count();
            $queryResult = $paginatorResult->getIterator()->getArrayCopy();
            $metadata = [
                'pages' => (int) \ceil($countTotal/$perPage),
                'page' => $page,
                'showing' => \count($queryResult),
                'total' => $countTotal,
            ];

            return new Response($queryResult, $metadata);
        } catch (\Doctrine\ORM\ORMException $e) {
            return null;
        }
    }
}
