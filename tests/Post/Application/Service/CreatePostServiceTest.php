<?php

/*
 * This file is part of the insided/post bounded context.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace Insided\Post\Application\Service;

use Insided\Common\Domain\IdentityGenerator;
use Insided\Common\Testing\SymfonyDoctrine\PHPUnitSymfonyDoctrineTestCase;
use Insided\Post\Application\Interaction\Command\CreatePostCommand;
use Insided\Post\Domain\Model\PostId;
use Insided\Post\Domain\Model\PostMessage;
use Insided\Post\Domain\Model\PostTitle;
use Insided\Post\Domain\Model\Repository\PostReaderRepository;

/**
 * @author Fernando Coca <fcocajimeno@gmail.com>
 */
class CreatePostServiceTest extends PHPUnitSymfonyDoctrineTestCase
{
    /**
     * @return string
     */
    protected function managerName(): string
    {
        return 'insided';
    }

    public function test_it_should_create_a_post()
    {
        $postId = new PostId(IdentityGenerator::generate());
        $postTitle = new PostTitle('post title');
        $postMessage  = new PostMessage('post message');

        $createPostCommand = new CreatePostCommand(
            $postId,
            $postTitle,
            $postMessage
        );

        $this->createPostService()->execute($createPostCommand);

        $postAdded = $this->postReaderRepository()->postOfId($postId);

        self::assertNotNull($postAdded);
        self::assertSame($postAdded->id()->id(), $postId->id());
    }

    private function postReaderRepository(): PostReaderRepository
    {
        return static::service('insided.post.repository.reader.post');
    }

    private function createPostService(): CreatePostService
    {
        return static::service('insided.post.app_service.create_post');
    }
}
