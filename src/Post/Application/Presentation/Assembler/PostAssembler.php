<?php

/*
 * This file is part of the insided/post bounded context.
 *
 * (c) Fernando Coca <fcocajimeno@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types = 1);

namespace Insided\Post\Application\Presentation\Assembler;

use Insided\Common\Domain\Model\AggregateRoot;
use Insided\Common\Presentation\Assembler;
use Insided\Common\Presentation\Representation;
use Insided\Post\Application\Presentation\Representation\PostRepresentation;
use Insided\Post\Domain\Model\Post;

/**
 * Normalizer: Post
 *
 * @author Fernando Coca <fcocajimeno@gmail.com>
 */
class PostAssembler implements Assembler
{
    /**
     * @param \Insided\Common\Domain\Model\AggregateRoot $post
     *
     * @return \Insided\Common\Presentation\Representation|null
     */
    public function assemble(AggregateRoot $post): ?Representation
    {
        if (! $post instanceof Post) {
            return null;
        }

        return new PostRepresentation(
            $post->id()->id(),
            $post->title()->title(),
            $post->message()->message(),
            $post->creationDate()->format('Y-m-d\TH:i:s')
        );
    }
}
